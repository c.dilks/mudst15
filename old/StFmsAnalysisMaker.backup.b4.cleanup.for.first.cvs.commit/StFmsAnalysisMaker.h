#ifndef STAR_StFmsAnalysisMaker_HH
#define STAR_StFmsAnalysisMaker_HH

#include "StMaker.h"
#include "StEnumerations.h"
#include "StLorentzVectorF.hh"
#include "StThreeVectorF.hh"
#include "TMatrix.h"
#include "TVector3.h"


class StMuDstMaker;
class StFmsDbMaker;
class StFmsCollection;
class StMuFmsCollection;
class StMuRpsCollection;
class StMuRpsCollection2;
class StMuRpsUtil;
class StSpinDbMaker;
class TClonesArray;
class StMuFmsCluster;
class StMuFmsPoint;
class StMuFmsHit;
class StFmsCluster;
class StFmsPoint;
class StFmsHit;
class StFmsPointPair;
class StMuRpsTrack;
class StMuRpsTrackPoint;
class TH2F;
class StMuDst;
class StMuEvent;
class StEvent;
//class TMatrixT;

#ifndef CONST_DEFINED
#define CONST_DEFINED

// FMS constants
const int NCELLS = 1264;
const Int_t nRows[4] = {34, 34, 24, 24}; // number of rows (for each nstb)
const Int_t nCols[4] = {17, 17, 12, 12}; // number of columns (for each nstb)
const Float_t cellWidth[4] = { 5.812, 
                               5.812, 
                               3.822, 
                               3.822 }; // cell width (cm) (from root12fms/FpdRoot::Geom class)
const Float_t zPos[4] = { 734.099976,
                          734.099976,
                          729.700012,
                          729.700012 }; // z-position of each nstb (from root12fms/FpdRoot::Geom class)
const Float_t xOffset[4] = { -0.30,
                              0.30,
                             -0.93,
                              0.93 }; // x offset (from root12fms/FpdRoot::Geom class)
const Float_t yOffset[4] = { 98.8,
                             98.8,
                             46.5,
                             46.5 }; // y offset (from root12fms/FpdRoot::Geom class)
             // (see heppellab16f:~/15tran/root12fms/PRINT_GEOM_TABLE.C)


// RP constants
const int n_tracks_max = 1000; // max number of tracks
const Double_t thetaRpLimits[2][2] = { {-1.5e-3, 5.0e-3},
                                       { 1.0e-3, 5.5e-3} }; // [rad]; theta limits from rafal


// other constants
const int MAX_POUT = NCELLS; // max size for p_out tree arrays

#endif

class StFmsAnalysisMaker : public StMaker{
  public: 
    StFmsAnalysisMaker(StMuDstMaker * maker, const Char_t* name="FmsOffQA");
    ~StFmsAnalysisMaker();
    Int_t Init();
    Int_t Make();
    Int_t Finish();

    void setFileName(char* file) { mFilename=file; };
    void setPrint(int v) { mPrint=v; };

    void ResetRpTrackVars(Int_t i_);
    void ResetRpTrackPointVars(Int_t i_);
    void ResetFmsBranchVars();

    static int NTracksMax() { return n_tracks_max; };

    // options
    Bool_t build_CompTr,build_evtr;
    Bool_t verbose,verbose_clu,verbose_pho,verbose_rp;
    Bool_t check_spinbyte;
    Bool_t dump_spinbits;


  // -------------------------------------------------------------------------------------------------
  private:

    // event and collection pointers =================================================================
    StSpinDbMaker * mSpinDb;
    StFmsDbMaker * mFmsDb;
    StMuFmsCollection * muFmsColl;
    //StMuRpsCollection * mMuRpsColl; // deprecated -- associated with pre-afterburner data //+++
    StMuRpsUtil * mAfterburner; // Afterburner (RP post-processing)
    StMuRpsCollection2 * mMuRpsColl; // associated with afterburner result (notice suffix "2") //+++

    StMuDst * muDst;
    StMuEvent * muEvent;
    StEvent * dsEvent;
    StFmsCollection * dsFmsColl;

    // StMuEvent object arrays
    TClonesArray * muHits;
    TClonesArray * muClusters;
    TObjArray * clusterArr[4];
    TClonesArray * muPoints;

    // event objects
    StFmsCluster * clu;
    StFmsPoint * pho;
    StFmsHit * hit;
    StMuFmsCluster * muclu;
    StFmsPointPair * pair;
    StMuFmsHit * muhit;
    StMuRpsTrack * trk;
    const StMuRpsTrackPoint * trkpnt;


    // tree for events (e.g., for event display) =====================================================
    TTree * evtr;

    UInt_t bc[2]; // bxing counter [0=lowBits,1=highBits]
    Int_t evid; // event id
    Long64_t BunchL; // bxing counter (same as bc, but it's a 'long int' instead)

    Int_t nhits,nclusters,npoints; // number of hits, clusters, points, pairs
    Int_t hc,cc,pc,ppc; // hit/cluster/point/pair counter (which may be less than nhits/nclusters/npoints)
    Int_t NmuHits,NmuClusters,NmuPoints; // number of hits/clusters/points from StMuEvent
    Int_t NdsHits,NdsClusters,NdsPoints,NdsPairs; // number of hits/clusters/points from StEvent
    Int_t NmuCluhits,NdsCluhits; // number of clusters' hits 

    Int_t nTriggers;
    Int_t trigids[128];

    // evtr branches for hits
    unsigned short hit_nstb[NCELLS];
    unsigned short hit_chan[NCELLS];
    Int_t hit_row[NCELLS];
    Int_t hit_col[NCELLS];
    unsigned short hit_adc[NCELLS];
    Float_t hit_en[NCELLS];
    Float_t gain[NCELLS];
    Float_t gainCorr[NCELLS];
    Short_t bitshift[NCELLS];
    unsigned short adcCorrected[NCELLS];
    unsigned short hit_tdc[NCELLS];
    //float timeDepCorr[NCELLS];

    // evtr branches for clusters
    Int_t clu_id[NCELLS];
    Float_t clu_en[NCELLS];
    Float_t clu_x[NCELLS];
    Float_t clu_y[NCELLS];
    Float_t clu_sigmin[NCELLS];
    Float_t clu_sigmax[NCELLS];
    Float_t clu_csqn1[NCELLS];
    Float_t clu_csqn2[NCELLS];
    Int_t clu_category[NCELLS];
    Int_t clu_ntowers[NCELLS];
    Int_t clu_nphotons[NCELLS];
    unsigned short clu_nstb[NCELLS];
    StThreeVectorF clu_pos;
    //Float_t clu_enFromHits[NCELLS];
    //Float_t clu_enFromPoints[NCELLS];

    // evtr branches for points
    Int_t pho_id[NCELLS];
    Float_t pho_en[NCELLS];
    Float_t pho_x[NCELLS];
    Float_t pho_y[NCELLS];
    Float_t pho_px[NCELLS];
    Float_t pho_py[NCELLS];
    Float_t pho_pz[NCELLS];
    unsigned short pho_nstb[NCELLS];
    StThreeVectorF pho_pos;
    StLorentzVectorF pho_mom;
    Int_t pho_fpsPid[NCELLS];

    // tree for point pairs
    TTree * pairtr;
    Int_t npairs;
    Float_t pair_en[NCELLS];
    Float_t pair_pt[NCELLS];
    Float_t pair_eta[NCELLS];
    Float_t pair_phi[NCELLS];
    Float_t pair_mass[NCELLS];
    Float_t pair_dgg[NCELLS];
    Float_t pair_zgg[NCELLS];
    Float_t pair_x[NCELLS];
    Float_t pair_y[NCELLS];
    Float_t pair_coneRad[3][NCELLS]; // [0=100mrad, 1=70mrad, 2=30mrad]
    Float_t pair_coneEn[3][NCELLS]; // [0=100mrad, 1=70mrad, 2=30mrad]
    Float_t pair_coneEnFrac[3][NCELLS]; // [0=100mrad, 1=70mrad, 2=30mrad]
    UInt_t pair_fpsPid[NCELLS];
   
    // *compTr: trees for comparing between StEvent and StMuEvent ========================

    // hitCompTr: tree for comparing hits betwen StEvent and StMuEvent
    TTree * hitCompTr; // for hits directly from hit list
    unsigned short hit_chanMU[NCELLS];
    unsigned short hit_nstbMU[NCELLS];
    float hit_energyMU[NCELLS];
    unsigned short hit_adcMU[NCELLS];
    unsigned short hit_chanDS[NCELLS];
    unsigned short hit_nstbDS[NCELLS];
    float hit_energyDS[NCELLS];
    unsigned short hit_adcDS[NCELLS];

    // cluhitCompTr: tree for comparing clusters' hits betwen StEvent and StMuEvent
    TTree * cluhitCompTr; // for hits within clusters from cluster list (called 'cluhits')
    unsigned short cluhit_chanMU[NCELLS];
    unsigned short cluhit_nstbMU[NCELLS];
    float cluhit_energyMU[NCELLS];
    unsigned short cluhit_adcMU[NCELLS];
    unsigned short cluhit_chanDS[NCELLS];
    unsigned short cluhit_nstbDS[NCELLS];
    float cluhit_energyDS[NCELLS];
    unsigned short cluhit_adcDS[NCELLS];


    // OFile p_out tree =======================================================================
    TTree * p_out;
    Int_t spin; 
    Int_t nphotons; 
    Int_t nwrds; 
    Int_t tpes[MAX_POUT]; 
    Float_t pxyzt[MAX_POUT]; 
    Int_t Rnum,Rnum_tmp;
    Int_t Bunchid7bit; 
    UChar_t BBcSums[5]; 
    Float_t BBcVertex[7]; 
    Int_t EventN; 
    Int_t ievt; 

    // p_out FMS branches
    Int_t nSavedHits; 
    UInt_t SavedHits[MAX_POUT]; 
    UChar_t SavedTDC[MAX_POUT];
    Float_t SavedCluTDC[MAX_POUT];
    Int_t TrigBits; 
    Int_t nCluster; 
    Int_t nPhotonClu; 
    Int_t SCIndex[MAX_POUT]; 
    Int_t SPCIndex[MAX_POUT]; 
    Float_t SPCEnergy[MAX_POUT]; 
    UInt_t L2sum[2]; 
    UInt_t lastdsm[8]; 
    UInt_t Fpde[8]; 

    // p_out FPS branches
    Int_t fpsPid[MAX_POUT];

    // p_out RP branches
    Int_t n_tracks,n_trackpoints;
    Int_t tc; // track counter
    Int_t tpc[n_tracks_max]; // trackpoint counter
    Int_t tpcFromTrackType;
    // track variables (prefixed with t_)
    Int_t t_index[n_tracks_max];
    Int_t t_branch[n_tracks_max];
    Int_t t_type[n_tracks_max];
    UInt_t t_planesUsed[n_tracks_max];
    Double_t t_p[n_tracks_max];
    Double_t t_pt[n_tracks_max];
    Double_t t_eta[n_tracks_max];
    Double_t t_time[n_tracks_max];
    Double_t t_theta[n_tracks_max][3]; // [track] [angle (X,Y,full)]
    Double_t t_thetaRP[n_tracks_max][3]; // [track] [angle (X,Y,full)]
    Double_t t_phi[n_tracks_max];
    Double_t t_phiRP[n_tracks_max];
    Double_t t_t[n_tracks_max];
    Double_t t_xi[n_tracks_max];
    Bool_t t_isBad[n_tracks_max];
    Double_t t_qualHash[n_tracks_max];
    // trackpoint variables (prefixed with p_) 
    Bool_t p_tpExists[n_tracks_max][2];
    Int_t p_RPid[n_tracks_max][2];
    Int_t p_clustid_s1[n_tracks_max][2]; 
    Int_t p_clustid_s2[n_tracks_max][2]; 
    Int_t p_clustid_s3[n_tracks_max][2]; 
    Int_t p_clustid_s4[n_tracks_max][2]; 
    Int_t p_quality[n_tracks_max][2];
    UInt_t p_planesUsed[n_tracks_max][2];
    Double_t p_x[n_tracks_max][2];
    Double_t p_y[n_tracks_max][2];
    Double_t p_z[n_tracks_max][2];
    Double_t p_time_pmt1[n_tracks_max][2];
    Double_t p_time_pmt2[n_tracks_max][2];

    //p_out misc branches
    unsigned short bbcADCSum[2];
    unsigned short bbcADCSumLarge[2];
    unsigned short bbcEarliestTDC[2];
    unsigned short bbcEarliestTDCLarge[2];
    unsigned short zdcADCSum[2];
    unsigned short vpdADCSum[2];
    unsigned short tofMultiplicity;
    //unsigned short mtdADC[2];

    

    
    // other variables ========================================================

    // miscellaneous
    char * mFilename;
    TFile * mFile;
    int mPrint;
    unsigned long long L2sum_full;

    // spin vars
    Int_t spinbit;
    Int_t bNib,yNib;
    Int_t spinbyte,spinbyte_db;
    Int_t spinbyte_curr,spinbyte_db_curr;
    Bool_t spinbyte_same;
    char spc_b,spc_y;
    Int_t sp_b,sp_y;
    Int_t spinFromSpinbyte;

    // FMS vars
    unsigned short detId;
    unsigned short nstb;
    unsigned short hitADC;
    unsigned short hitTDC;
    Float_t cluSumTDC,cluAveTDC;
    Int_t cluHitCount;
    int hit_r,hit_c,hit_n;
    Float_t cluEn;
    Int_t ent[4];
    Float_t en_sum_all;
    Float_t en_sum[4];
    Float_t clu_en_sortval[MAX_POUT];
    Float_t EnergySum,Energy;
    Int_t clu_en_sortind[MAX_POUT];
    StLorentzVectorF vecs[MAX_POUT];
    Int_t num_null;
    TMatrix * eMat[4]; // [nstb]
    unsigned int p1,p2,p3,p4,pa;

    // RP vars
    //Int_t trkSortIdx[n_tracks_max];
    //Double_t trkSortQual[n_tracks_max];
    Double_t trkMom;
    Int_t trkPlanes;
    Int_t trkGlobal;
    Int_t trkInGeom;
    Double_t trkThetaX;
    Double_t trkThetaY;


    

    virtual const char * GetCVS() const
    {
      static const char cvs[] = 
      "Tag $Name:  $ $Id: StFmsAnalysisMaker.h,v 1.5 2015/12/08 17:00:03 akio Exp $ built " __DATE__ " " __TIME__ ;
      return cvs;
    };

    ClassDef(StFmsAnalysisMaker,0);
};

#endif
