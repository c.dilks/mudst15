/* Execute MuDST-->MFile algorithm */


#include "TString.h"
#include "TROOT.h"
#include "TSystem.h"
#include <StChain>

//void runMudst4(const char* file="st_fms_16069001_raw_2500015.MuDst.root",Int_t runnum=16069001,
void runMudst4(const char* file="st_fms_16088020_raw_5500016.MuDst.root",Int_t runnum=16088020,
               const char * jobid="single", 
               Int_t ifile=-1,
               Int_t nevt=10000
              ) {
  // sanity check
  printf("file=%s\n",file);
  printf("runnum=%d\n",runnum);
  printf("jobid=%s\n",jobid);
  printf("ifile=%d\n",ifile);
  printf("nevt=%d\n",nevt);

  // load libraries
  gROOT->Macro("load.C");
  gSystem->Load("StEventMaker");


  // instantiate chain and mudst maker
  StChain* chain = new StChain("StChain"); chain->SetDEBUG(0);
  printf("--------------------------- error below here\n");
  StMuDstMaker* muDstMaker = new StMuDstMaker(0, 0, "", file,".", 1000, "MuDst");
  // constructor arguments
  //  -- int mode -- 0=read; 1=write
  //  -- int nameMode -- 0=read from inFile; 1=ioMaker
  //  -- char * dirName -- 
  //  -- char * fileName -- name of .root or filelist (pass to macro)
  //  -- char * filter -- 
  //  -- int maxFiles -- max # of root files to chain
  //  -- char * name -- 
  printf("--------------------------- error above here\n");
  int n=muDstMaker->tree()->GetEntries();
  printf("Found %d entries in Mudst\n",n);
  int start=0, stop=n;
  if(ifile>=0){
    int start=ifile*nevt;
    int stop=(ifile+1)*nevt-1;
    if(n<start) {printf(" No event left. Exiting\n"); return;}
    if(n<stop)  {printf(" Overwriting end event# stop=%d\n",n); stop=n;}
  }else if(nevt>=0 && nevt<n){
    stop=nevt;
  }else if(nevt==-2){
    stop=2000000000; 
  }
  printf("Doing Event=%d to %d\n",start,stop);



  // instantiate databases
  St_db_Maker* dbMk = new St_db_Maker("db","MySQL:StarDb","$STAR/StarDb"); 
  //dbMk->SetDEBUG(2); //dbMk->SetDateTime(20150301,0);

  StFmsDbMaker* fmsdb = new StFmsDbMaker("fmsDb");  
  //fmsdb->setDebug(1); 
  //fmsdb->readGainFromText();
  //fmsdb->readRecParamFromFile();
  
  StSpinDbMaker * spindb = new StSpinDbMaker("spinDb");
  //spindb->InitRun(runnum);



  // instantiate analysis maker and define its outputs
  gSystem->Load("StFmsAnalysisMaker");
  StFmsAnalysisMaker* fmsan = new StFmsAnalysisMaker(muDstMaker,spindb);
  TString filenameFmsAnalysisMaker(file);
  TRegexp re("\/.*\/");
  filenameFmsAnalysisMaker(re) = "";
  filenameFmsAnalysisMaker.ReplaceAll("sched",Form("%d.",runnum));
  filenameFmsAnalysisMaker.ReplaceAll("list","fmsan.root");
  filenameFmsAnalysisMaker.ReplaceAll("MuDst","fmsan");

  cout << "FmsAnalysisMaker outfile name = " << filenameFmsAnalysisMaker.Data()<<endl;
  fmsan->setFileName(filenameFmsAnalysisMaker.Data());
  fmsan->setPrint(1);



  // execute chain
  chain->Init();
  chain->EventLoop(start,stop);
  chain->Finish();
  delete chain;
}

#ifndef __CINT__
int main(int argc, char ** argv) {
  switch(argc) {
    case 1: 
      runMudst5();
      break;
    case 2:
      runMudst5(argv[1]);
      break;
    case 3:
      runMudst5(argv[1],
                (Int_t)strtof(argv[2],NULL)
               );
      break;
    case 4:
      runMudst5(argv[1],
                (Int_t)strtof(argv[2],NULL),
                argv[3]
               );
      break;
    case 5:
      runMudst5(argv[1],
                (Int_t)strtof(argv[2],NULL),
                argv[3],
                (Int_t)strtof(argv[4],NULL)
               );
      break;
    case 6:
      runMudst5(argv[1],
                (Int_t)strtof(argv[2],NULL),
                argv[3],
                (Int_t)strtof(argv[4],NULL),
                (Int_t)strtof(argv[5],NULL)
               );
      break;
    default:
      fprintf(stderr,"ERROR: TOO MANY ARGUMENTS\n");
      return 0;
  };
  return 1;
};
#endif
