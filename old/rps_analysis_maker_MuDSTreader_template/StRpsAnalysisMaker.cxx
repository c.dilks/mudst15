// Based on the MuDST tools written by Frank Laue.
// Based on the DST Tutorial by Dan Magestro on the STAR Computing/Tutorials page.
// Updated 9/4/2006 by Jim Thomas to include the latest DST format, and Scheduler techniques.

#include "StRpsAnalysisMaker.h"

#include <iostream>

#include "StMuDSTMaker/COMMON/StMuDstMaker.h"
#include "StMuDSTMaker/COMMON/StMuTrack.h"
#include "StMuDSTMaker/COMMON/StMuEvent.h"
#include "StMuDSTMaker/COMMON/StMuRpsCollection.h"
#include "StMuDSTMaker/COMMON/StMuRpsTrackPoint.h"
#include "StMuDSTMaker/COMMON/StMuRpsTrack.h"
#include "StMuDSTMaker/COMMON/StMuTofHit.h"

#include "TH1.h"
#include "TH2.h"
#include "TFile.h"
#include "TObjArray.h"
#include "TList.h"
#include "TTree.h"

ClassImp(StRpsAnalysisMaker)


StRpsAnalysisMaker::StRpsAnalysisMaker( StMuDstMaker* maker ) : StMaker("StRpsAnalysisMaker") {
  mMuDstMaker = maker;
  fTFile = nullptr;
  mEventsProcessed = 0;
  mHistogramOutputFileName = "";
}


StRpsAnalysisMaker::~StRpsAnalysisMaker() {}


Int_t StRpsAnalysisMaker::Init( ) {
  fTFile = new TFile(mHistogramOutputFileName,"RECREATE");

  const char RP[8][5] = { "E1U", "E1D", "E2U", "E2D", "W1U", "W1D", "W2U", "W2D" }; // 2015
  const char branch[4][3] = {"EU", "ED", "WU", "WD"};


  // analysis tree
  rptr = new TTree("rptr","rptr");
  rptr->Branch("evid",&evid,"evid/I");
  rptr->Branch("n_tracks",&n_tracks,"n_tracks/I");
  rptr->Branch("n_trackpoints",&n_trackpoints,"n_trackpoints/I");


  ///////////////////////////////////////////////////
  //
  
  // general
  hNumberOfTrackPoints = new TH1F("NumberOfTrackPoints", "NumberOfTrackPoints" , 1001, -0.5, 1000.5);
  hNumberOfTracks = new TH1F("NumberOfTracks", "NumberOfTracks" , 1001, -0.5, 1000.5);

  // tracks
  for(Int_t j=0; j<4; ++j){
    hXi_globalTracks[j] = new TH1F("Xi_globalTracks_"+TString(branch[j]), "#xi of proton (globalTracks), branch "+TString(branch[j]), 2000, -1.0, 1.0);
    hXi_localTracks[j] = new TH1F("Xi_localTracks_"+TString(branch[j]), "#xi of proton (localTracks), branch "+TString(branch[j]), 2000, -1.0, 1.0);
  }

  for(Int_t j=0; j<4; ++j){
    hTransfer_globalTracks[j] = new TH1F("Transfer_globalTracks_"+TString(branch[j]), "-t of proton (globalTracks), branch "+TString(branch[j]), 5000, -10, 10);
    hTransfer_localTracks[j] = new TH1F("Transfer_localTracks_"+TString(branch[j]), "-t of proton (localTracks), branch "+TString(branch[j]), 5000, -10, 10);
  }

  for(Int_t j=0; j<4; ++j){
    hEta_globalTracks[j] = new TH1F("hEta_globalTracks_"+TString(branch[j]), "hEta (globalTracks), branch "+TString(branch[j]), 1000, j<2?-20:0, j<2?0:20);
    hEta_localTracks[j] = new TH1F("hEta_localTracks_"+TString(branch[j]), "hEta (localTracks), branch "+TString(branch[j]), 1000, j<2?-20:0, j<2?0:20);
  }

  for(Int_t j=0; j<4; ++j){
    hXY_globalTracks[j][0] = new TH2F("hXY_RP1_globalTracks_"+TString(branch[j]), "hXY_RP1 (globalTracks), branch "+TString(branch[j]), 1000, -0.10, 0.10, 1000, -0.10, 0.10);
    hXY_localTracks[j][0] = new TH2F("hXY_RP1_localTracks_"+TString(branch[j]), "hXY_RP1 (localTracks), branch "+TString(branch[j]), 1000, -0.10, 0.10, 1000, -0.10, 0.10);
    hXY_globalTracks[j][1] = new TH2F("hXY_RP2_globalTracks_"+TString(branch[j]), "hXY_RP2 (globalTracks), branch "+TString(branch[j]), 1000, -0.10, 0.10, 1000, -0.10, 0.10);
    hXY_localTracks[j][1] = new TH2F("hXY_RP2_localTracks_"+TString(branch[j]), "hXY_RP2 (localTracks), branch "+TString(branch[j]), 1000, -0.10, 0.10, 1000, -0.10, 0.10);
  }


  for(Int_t j=0; j<4; ++j){
    hP_globalTracks[j][0] = new TH1F("hP_x_globalTracks_"+TString(branch[j]), "hP_x (globalTracks), branch "+TString(branch[j]), 2000, -10, 10);
    hP_localTracks[j][0] = new TH1F("hP_x_localTracks_"+TString(branch[j]), "hP_x (localTracks), branch "+TString(branch[j]), 2000, -10, 10);
    hP_globalTracks[j][1] = new TH1F("hP_y_globalTracks_"+TString(branch[j]), "hP_y (globalTracks), branch "+TString(branch[j]), 2000, -10, 10);
    hP_localTracks[j][1] = new TH1F("hP_y_localTracks_"+TString(branch[j]), "hP_y (localTracks), branch "+TString(branch[j]), 2000, -10, 10);
    hP_globalTracks[j][2] = new TH1F("hP_z_globalTracks_"+TString(branch[j]), "hP_z (globalTracks), branch "+TString(branch[j]),  5000, j<2?-150:50, j<2?-50:150);
    hP_localTracks[j][2] = new TH1F("hP_z_localTracks_"+TString(branch[j]), "hP_z (localTracks), branch "+TString(branch[j]), 5000, j<2?-150:50, j<2?-50:150);
  }

  for(Int_t j=0; j<4; ++j){
    hPval_globalTracks[j] = new TH1F("hPval_globalTracks_"+TString(branch[j]), "hPval (globalTracks), branch "+TString(branch[j]), 1000, 0, 200);
    hPval_localTracks[j] = new TH1F("hPval_localTracks_"+TString(branch[j]), "hPval (localTracks), branch "+TString(branch[j]), 1000, 0, 200);
  }

  for(Int_t j=0; j<4; ++j){
    hPt_globalTracks[j] = new TH1F("hPt_globalTracks_"+TString(branch[j]), "hPt (globalTracks), branch "+TString(branch[j]), 1000, 0, 10);
    hPt_localTracks[j] = new TH1F("hPt_localTracks_"+TString(branch[j]), "hPt (localTracks), branch "+TString(branch[j]), 1000, 0, 10);
  }

  for(Int_t j=0; j<4; ++j){
    hTheta_globalTracks[j][0] = new TH1F("hTheta_x_globalTracks_"+TString(branch[j]), "hTheta_x (globalTracks), branch "+TString(branch[j]), 2000, -0.010, 0.010);
    hTheta_localTracks[j][0] = new TH1F("hTheta_x_localTracks_"+TString(branch[j]), "hTheta_x (localTracks), branch "+TString(branch[j]), 2000, -0.010, 0.010);
    hTheta_globalTracks[j][1] = new TH1F("hTheta_y_globalTracks_"+TString(branch[j]), "hTheta_y (globalTracks), branch "+TString(branch[j]), 2000, -0.010, 0.010);
    hTheta_localTracks[j][1] = new TH1F("hTheta_y_localTracks_"+TString(branch[j]), "hTheta_y (localTracks), branch "+TString(branch[j]), 2000, -0.010, 0.010);
    hTheta_globalTracks[j][2] = new TH1F("hTheta_globalTracks_"+TString(branch[j]), "hTheta (globalTracks), branch "+TString(branch[j]), 1000, 0, 0.010);
    hTheta_localTracks[j][2] = new TH1F("hTheta_localTracks_"+TString(branch[j]), "hTheta (localTracks), branch "+TString(branch[j]), 1000, 0, 0.010);
  }


  for(Int_t j=0; j<4; ++j){
    hThetaRp_globalTracks[j][0] = new TH1F("hThetaRp_x_globalTracks_"+TString(branch[j]), "hThetaRp_x (globalTracks), branch "+TString(branch[j]), 2000, -0.010, 0.010);
    hThetaRp_localTracks[j][0] = new TH1F("hThetaRp_x_localTracks_"+TString(branch[j]), "hThetaRp_x (localTracks), branch "+TString(branch[j]), 2000, -0.010, 0.010);
    hThetaRp_globalTracks[j][1] = new TH1F("hThetaRp_y_globalTracks_"+TString(branch[j]), "hThetaRp_y (globalTracks), branch "+TString(branch[j]), 2000, -0.010, 0.010);
    hThetaRp_localTracks[j][1] = new TH1F("hThetaRp_y_localTracks_"+TString(branch[j]), "hThetaRp_y (localTracks), branch "+TString(branch[j]), 2000, -0.010, 0.010);
    hThetaRp_globalTracks[j][2] = new TH1F("hThetaRp_globalTracks_"+TString(branch[j]), "hThetaRp (globalTracks), branch "+TString(branch[j]), 1000, 0, 0.010);
    hThetaRp_localTracks[j][2] = new TH1F("hThetaRp_localTracks_"+TString(branch[j]), "hThetaRp (localTracks), branch "+TString(branch[j]), 1000, 0, 0.010);
  }


  for(Int_t j=0; j<4; ++j){
    hPhi_globalTracks[j] = new TH1F("hPhi_globalTracks_"+TString(branch[j]), "hPhi (globalTracks), branch "+TString(branch[j]), 1000, -3.14159, 3.14159);
    hPhi_localTracks[j] = new TH1F("hPhi_localTracks_"+TString(branch[j]), "hPhi (localTracks), branch "+TString(branch[j]), 1000, -3.14159, 3.14159);
  }

  for(Int_t j=0; j<4; ++j){
    hPhiRp_globalTracks[j] = new TH1F("hPhiRp_globalTracks_"+TString(branch[j]), "hPhiRp (globalTracks), branch "+TString(branch[j]), 1000, -3.14159, 3.14159);
    hPhiRp_localTracks[j] = new TH1F("hPhiRp_localTracks_"+TString(branch[j]), "hPhiRp (localTracks), branch "+TString(branch[j]), 1000, -3.14159, 3.14159);
  }

  for(Int_t j=0; j<4; ++j){
    hTime_globalTracks[j] = new TH1F("hTime_globalTracks_"+TString(branch[j]), "hTime (globalTracks), branch "+TString(branch[j]), 1000, 0, 50e-9);
    hTime_localTracks[j] = new TH1F("hTime_localTracks_"+TString(branch[j]), "hTime (localTracks), branch "+TString(branch[j]), 1000, 0, 50e-9);
  }

  for(Int_t j=0; j<4; ++j){
    hBranch_globalTracks[j] = new TH1F("hBranch_globalTracks_"+TString(branch[j]), "hBranch (globalTracks), branch "+TString(branch[j]), 6, -1, 5);
    hBranch_localTracks[j] = new TH1F("hBranch_localTracks_"+TString(branch[j]), "hBranch (localTracks), branch "+TString(branch[j]), 6, -1, 5);
  }

  for(Int_t j=0; j<4; ++j){
    hTrackType_globalTracks[j] = new TH1F("hTrackType_globalTracks_"+TString(branch[j]), "hTrackType (globalTracks), branch "+TString(branch[j]), 6, -1, 5);
    hTrackType_localTracks[j] = new TH1F("hTrackType_localTracks_"+TString(branch[j]), "hTrackType (localTracks), branch "+TString(branch[j]), 6, -1, 5);
  }

  for(Int_t j=0; j<4; ++j){
    hPlanesUsed_globalTracks[j] = new TH1F("hPlanesUsed_globalTracks_"+TString(branch[j]), "hPlanesUsed (globalTracks), branch "+TString(branch[j]), 10, -1, 9);
    hPlanesUsed_localTracks[j] = new TH1F("hPlanesUsed_localTracks_"+TString(branch[j]), "hPlanesUsed (localTracks), branch "+TString(branch[j]), 10, -1, 9);
  }

  for(Int_t j=0; j<4; ++j){
    hLocalAngle2D_globalTracks[j] = new TH2F("LocalAngle2D_globalTracks_"+TString(branch[j]), "hLocalAngle2D (globalTracks), branch "+TString(branch[j]), 1000, -0.010, 0.010, 1000, -0.010, 0.010);
    hLocalAngle2D_localTracks[j] = new TH2F("LocalAngle2D_localTracks_"+TString(branch[j]), "hLocalAngle2D (localTracks), branch "+TString(branch[j]), 1000, -0.010, 0.010, 1000, -0.010, 0.010);
  }


  // track-points
  for(Int_t j=0; j<8; ++j){
    hTime_trackPoints[j][0] = new TH1F("hTime_trackPoints_PMT0_"+TString(RP[j]), "hTime_trackPoints, "+TString(RP[j])+", PMT-0", 1000, 0, 50e-9);
    hTime_trackPoints[j][1] = new TH1F("hTime_trackPoints_PMT1_"+TString(RP[j]), "hTime_trackPoints, "+TString(RP[j])+", PMT-1", 1000, 0, 50e-9);
  }

  for(Int_t j=0; j<8; ++j){
    hClusterId_trackPoints[j][0] = new TH1F("hClusterId_trackPoints_PlaneA_"+TString(RP[j]), "hClusterId_trackPoints, "+TString(RP[j])+", Plane A", 100, 0, 100);
    hClusterId_trackPoints[j][1] = new TH1F("hClusterId_trackPoints_PlaneB_"+TString(RP[j]), "hClusterId_trackPoints, "+TString(RP[j])+", Plane B", 100, 0, 100);
    hClusterId_trackPoints[j][2] = new TH1F("hClusterId_trackPoints_PlaneC_"+TString(RP[j]), "hClusterId_trackPoints, "+TString(RP[j])+", Plane C", 100, 0, 100);
    hClusterId_trackPoints[j][3] = new TH1F("hClusterId_trackPoints_PlaneD_"+TString(RP[j]), "hClusterId_trackPoints, "+TString(RP[j])+", Plane D", 100, 0, 100);
  }

  for(Int_t j=0; j<8; ++j){
    hRpId_trackPoints[j] = new TH1F("hRpId_trackPoints_"+TString(RP[j]), "hRpId_trackPoints, "+TString(RP[j]), 9, -1, 8);
  }

  for(Int_t j=0; j<8; ++j){
    hQuality_trackPoints[j] = new TH1F("hQuality_trackPoints_"+TString(RP[j]), "hQuality_trackPoints, "+TString(RP[j]), 4, -1, 3);
  }

  for(Int_t j=0; j<8; ++j){
    hPlanesUsed_trackPoints[j] = new TH1F("hPlanesUsed_trackPoints_"+TString(RP[j]), "hPlanesUsed_trackPoints, "+TString(RP[j]), 6, -1, 5);
  }

  for(Int_t j=0; j<8; ++j){
    hXY_trackPoints[j] = new TH2F("hXY_trackPoints_"+TString(RP[j]), "hXY_trackPoints, "+TString(RP[j]), 1000, -0.10, 0.10, 1000, -0.10, 0.10);
  }

  for(Int_t j=0; j<8; ++j){
    hZ_trackPoints[j] = new TH1F("hZ__trackPoints_"+TString(RP[j]), "hZ__trackPoints, "+TString(RP[j]), 3000, j<4?-18:15, j<4?-15:18);
  }

  //
  ///////////////////////////////////////////////////


  return kStOK;
}


Int_t StRpsAnalysisMaker::Make() {

  StMuEvent* muEvent = mMuDstMaker->muDst()->event();
  StRunInfo runInfo = muEvent->runInfo();
  StMuRpsCollection* muRpsColl = mMuDstMaker->muDst()->RpsCollection();


  StMuTofHit *tofHit = mMuDstMaker->muDst()->tofHit(0);
  if(tofHit) LOG_INFO << tofHit->adc() << endm;

  // Retrieving tracks
  //  unsigned int nTrackPoints = muRpsColl->numberOfTrackPoints();
  //  unsigned int nTracks = muRpsColl->numberOfTracks();

  unsigned int nTrackPoints = 0;
  while( muRpsColl->trackPoint(nTrackPoints) ) ++nTrackPoints;
  unsigned int nTracks = 0;
  while( muRpsColl->track(nTracks) ) ++nTracks;


  hNumberOfTracks->Fill(nTracks);
  hNumberOfTrackPoints->Fill(nTrackPoints);

  evid = muEvent->eventId();
  n_tracks = nTracks;
  n_trackpoints = nTrackPoints;
  rptr->Fill();


  // track loop
  //for(int i=0; i<nTracks; i++) {
    //StMuRpsTrack




  ///////////////////////////////////////////////////
  //

  // loop over tracks
  for ( UInt_t i = 0; i < nTracks; ++i) {
    StMuRpsTrack *trk = muRpsColl->track(i);
    int branch = trk->branch();

    // GLOBAL
    if(trk->type()==StMuRpsTrack::rpsGlobal){
      hXi_globalTracks[branch]->Fill( trk->xi( runInfo.beamEnergy( trk->branch()<2 ? StBeamDirection::east : StBeamDirection::west ) ) );
      hTransfer_globalTracks[branch]->Fill( -trk->t( runInfo.beamEnergy( trk->branch()<2 ? StBeamDirection::east : StBeamDirection::west ) ) );
      hPval_globalTracks[branch]->Fill( trk->p() );
      hPt_globalTracks[branch]->Fill( trk->pt() );
      hEta_globalTracks[branch]->Fill( trk->eta() );
      for(int c=0; c<3; ++c){
        hP_globalTracks[branch][c]->Fill( trk->pVec()[c] );
        hTheta_globalTracks[branch][c]->Fill( trk->theta(c) );
        hThetaRp_globalTracks[branch][c]->Fill( trk->thetaRp(c) );
      }
      for(int st=0; st<2; ++st){
        if( trk->trackPoint(st) ){
          hXY_globalTracks[branch][st]->Fill( trk->trackPoint(st)->x(), trk->trackPoint(st)->y() );
        }
      }
      hPhi_globalTracks[branch]->Fill( trk->phi() );
      hPhiRp_globalTracks[branch]->Fill( trk->phiRp() );
      hBranch_globalTracks[branch]->Fill( trk->branch() );
      hTrackType_globalTracks[branch]->Fill( trk->type() );
      hPlanesUsed_globalTracks[branch]->Fill( trk->planesUsed() );
      hTime_globalTracks[branch]->Fill( trk->time() );
      hLocalAngle2D_globalTracks[branch]->Fill( trk->thetaRp(0), trk->thetaRp(1) );
    }
    else
      // LOCAL
      if(trk->type()==StMuRpsTrack::rpsLocal){
        hXi_localTracks[branch]->Fill( trk->xi( runInfo.beamEnergy( trk->branch()<2 ? StBeamDirection::east : StBeamDirection::west ) ) );
        hTransfer_localTracks[branch]->Fill( -trk->t( runInfo.beamEnergy( trk->branch()<2 ? StBeamDirection::east : StBeamDirection::west ) ) );
        hPval_localTracks[branch]->Fill( trk->p() );
        hPt_localTracks[branch]->Fill( trk->pt() );
        hEta_localTracks[branch]->Fill( trk->eta() );
        for(int c=0; c<3; ++c){
          hP_localTracks[branch][c]->Fill( trk->pVec()[c] );
          hTheta_localTracks[branch][c]->Fill( trk->theta(c) );
          hThetaRp_localTracks[branch][c]->Fill( trk->thetaRp(c) );
        }
        for(int st=0; st<2; ++st){
          if( trk->trackPoint(st) ){
            hXY_localTracks[branch][st]->Fill( trk->trackPoint(st)->x(), trk->trackPoint(st)->y() );
          }
        }
        hPhi_localTracks[branch]->Fill( trk->phi() );
        hPhiRp_localTracks[branch]->Fill( trk->phiRp() );
        hBranch_localTracks[branch]->Fill( trk->branch() );
        hTrackType_localTracks[branch]->Fill( trk->type() );
        hPlanesUsed_localTracks[branch]->Fill( trk->planesUsed() );
        hTime_localTracks[branch]->Fill( trk->time() );
        hLocalAngle2D_localTracks[branch]->Fill( trk->thetaRp(0), trk->thetaRp(1) );
      }

  }


  // loop over track-points
  for ( UInt_t i = 0; i < nTrackPoints; ++i) {
    StMuRpsTrackPoint *trkPt = muRpsColl->trackPoint(i);
    int rp = trkPt->rpId();
    for ( UInt_t pmt = 0; pmt < trkPt->mNumberOfPmtsInRp; ++pmt) hTime_trackPoints[rp][pmt]->Fill( trkPt->time(pmt) );
    for ( UInt_t plane = 0; plane < trkPt->mNumberOfPlanesInRp; ++plane) hClusterId_trackPoints[rp][plane]->Fill( trkPt->clusterId(plane) );
    hRpId_trackPoints[rp]->Fill( rp );
    hQuality_trackPoints[rp]->Fill( trkPt->quality() );
    hPlanesUsed_trackPoints[rp]->Fill( trkPt->planesUsed() );
    hXY_trackPoints[rp]->Fill( trkPt->x(), trkPt->y() );
    hZ_trackPoints[rp]->Fill( trkPt->z() );
  }

  //
  ///////////////////////////////////////////////////

  mEventsProcessed++ ;
  return kStOK ;
}


Int_t StRpsAnalysisMaker::Finish() {
  fTFile->Write();
  LOG_INFO << "Total Events Processed in DstMaker " << mEventsProcessed << endm;
  return kStOK;
}









