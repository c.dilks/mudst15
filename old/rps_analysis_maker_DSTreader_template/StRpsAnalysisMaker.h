// Based on Rafal Sikora's analysis maker
// Based on the MuDST tools written by Frank Laue.
// Based on the DST Tutorial by Dan Magestro on the STAR Computing/Tutorials page.
// Updated 9/4/2006 by Jim Thomas to include the latest DST format, and Scheduler techniques.

#ifndef StRpsAnalysisMaker_def
#define StRpsAnalysisMaker_def

#include "StMaker.h"
#include "TString.h"
//#include "StContainers.h"

class StMuDstMaker;
class TFile;
class TH1F;
class TH2F;

class StRpsAnalysisMaker : public StMaker {

  public:

    //StRpsAnalysisMaker(StMuDstMaker* maker);
    StRpsAnalysisMaker();
    virtual ~StRpsAnalysisMaker();

    Int_t Init();
    Int_t Make();
    Int_t Finish();

    void SetOutputFileName(TString name) { mHistogramOutputFileName = name; };

  private:


    StMuDstMaker * mMuDstMaker;
    TFile * fTFile;
    UInt_t mEventsProcessed; //  Number of Events read and processed
    TString mHistogramOutputFileName; //  Name of the histogram output file 

    TTree * rptr;
    Int_t evid,n_tracks,n_trackpoints;

    //StPtrVecRpsTrackPoint mTrackPoints;
    //StPtrVecRpsTrack mTracks;


    ///////////////////////////////////////////////////
    //
    // ---------- Histograms ----------
    // tracks
    TH2F * hXY_globalTracks[4][2];
    TH2F * hXY_localTracks[4][2];
    TH1F * hXi_globalTracks[4];
    TH1F * hXi_localTracks[4];
    TH1F * hTransfer_globalTracks[4];
    TH1F * hTransfer_localTracks[4];
    TH1F * hEta_globalTracks[4];
    TH1F * hEta_localTracks[4];
    TH1F * hP_globalTracks[4][3];
    TH1F * hP_localTracks[4][3];
    TH1F * hPval_globalTracks[4];
    TH1F * hPval_localTracks[4];
    TH1F * hPt_globalTracks[4];
    TH1F * hPt_localTracks[4];
    TH1F * hTheta_globalTracks[4][3];
    TH1F * hTheta_localTracks[4][3];
    TH1F * hThetaRp_globalTracks[4][3];
    TH1F * hThetaRp_localTracks[4][3];
    TH1F * hPhi_globalTracks[4];
    TH1F * hPhi_localTracks[4];
    TH1F * hPhiRp_globalTracks[4];
    TH1F * hPhiRp_localTracks[4];
    TH1F * hTime_globalTracks[4];
    TH1F * hTime_localTracks[4];
    TH1F * hBranch_globalTracks[4];
    TH1F * hBranch_localTracks[4];
    TH1F * hTrackType_globalTracks[4];
    TH1F * hTrackType_localTracks[4];
    TH1F * hPlanesUsed_globalTracks[4];
    TH1F * hPlanesUsed_localTracks[4];
    TH2F * hLocalAngle2D_globalTracks[4];
    TH2F * hLocalAngle2D_localTracks[4];

    // track-points
    TH1F * hTime_trackPoints[8][2];
    TH1F * hClusterId_trackPoints[8][4];
    TH1F * hRpId_trackPoints[8];
    TH1F * hQuality_trackPoints[8];
    TH1F * hPlanesUsed_trackPoints[8];
    TH2F * hXY_trackPoints[8];
    TH1F * hZ_trackPoints[8];

    // general
    TH1F * hNumberOfTrackPoints;
    TH1F * hNumberOfTracks;
    //
    ///////////////////////////////////////////////////


    ClassDef(StRpsAnalysisMaker,1)
};

#endif
