// \class StFmsAnalysisMaker
// \author Akio Ogawa
//
//  $Id: StFmsAnalysisMaker.cxx,v 1.6 2015/12/08 17:00:03 akio Exp $
//

#include "StFmsAnalysisMaker.h"

#include "StMessMgr.h"
#include "Stypes.h"

#include "StFmsDbMaker/StFmsDbMaker.h"
#include "StEnumerations.h"
#include "StEventTypes.h"
#include "StEvent/StEvent.h"

#include "StEvent/StFmsCollection.h"
#include "StEvent/StFmsHit.h"
#include "StEvent/StFmsPoint.h"
#include "StEvent/StFmsPointPair.h"

#include "StEvent/StRpsCollection.h"
#include "StEvent/StRpsTrackPoint.h"
#include "StEvent/StRpsTrack.h"

#include "StMuDSTMaker/COMMON/StMuTypes.hh"

#include "TFile.h"
#include "TH1F.h"
#include "TTree.h"
#include "TH2F.h"

ClassImp(StFmsAnalysisMaker);

StFmsAnalysisMaker::StFmsAnalysisMaker(const Char_t* name):
  StMaker(name),mFilename((char *)"fmsOffQa.root"),mPrint(1)
{}

StFmsAnalysisMaker::~StFmsAnalysisMaker(){}

Int_t StFmsAnalysisMaker::Init(){  
  mFmsDbMaker=static_cast<StFmsDbMaker*>(GetMaker("fmsDb"));  
  if(!mFmsDbMaker){
    LOG_ERROR  << "StFmsAnalysisMaker::InitRun Failed to get StFmsDbMaker" << endm;
    return kStFatal;
  }    
  mFile=new TFile(mFilename,"RECREATE");

  
  // event tree
  evtr = new TTree("evtr","evtr");
  evtr->Branch("bc",bc,"bc[2]/i"); // bunch crossing counter
  evtr->Branch("evid",&evid,"evid/I"); // event id
  evtr->Branch("nh",&nh,"nh/I"); // number of hits
  evtr->Branch("nc",&nc,"nc/I"); // number of clusters
  evtr->Branch("np",&np,"np/I"); // number of points
  evtr->Branch("npair",&npair,"npair/I"); // number of point pairs

  
  // cluster tree
  clustr = new TTree("clustr","clustr");
  clustr->Branch("bc",bc,"bc[2]/i"); // bunch crossing counter
  clustr->Branch("evid",&evid,"evid/I"); // event id
  clustr->Branch("ls",&llss,"ls/I"); // 0=large 1=small
  clustr->Branch("det",&det_ctr,"det/I"); // detector ID
  clustr->Branch("category",&category_ctr,"category/I"); // cluster category ??
  clustr->Branch("ntow",&ntow_ctr,"ntow/I"); // number of towers in cluster
  clustr->Branch("nphot",&nphot_ctr,"nphot/I"); // number of photons
  clustr->Branch("en",&en_ctr,"en/F"); // energy 
  clustr->Branch("pt",&pt_ctr,"pt/F"); // transverse momentum 
  clustr->Branch("x",&x_ctr,"x/F"); // x position
  clustr->Branch("y",&y_ctr,"y/F"); // y position
  clustr->Branch("sigmax",&sigmax_ctr,"sigmax/F"); // max eigenvalue of 2-moment of energy matrix
  clustr->Branch("sigmin",&sigmin_ctr,"sigmin/F"); // min eigenvalue of 2-moment of energy matrix
  clustr->Branch("chi2ndf_onephot",&chi2ndf_onephot_ctr,"chi2ndf_onephot/F"); // chi2/NDF for 1-photon fit
  clustr->Branch("chi2ndf_twophot",&chi2ndf_twophot_ctr,"chi2ndf_twophot/F"); // chi2/NDF for 2-photon fit
  clustr->Branch("id",&id_ctr,"id/I"); // cluster ID ??

  
  // pair tree
  pairtr = new TTree("pairtr","pairtr");
  pairtr->Branch("bc",bc,"bc[2]/i"); // bunch crossing counter
  pairtr->Branch("evid",&evid,"evid/I"); // event id
  pairtr->Branch("ls",&llss,"ls/I"); // 0=large 1=small
  pairtr->Branch("nclust_in_pair",&nclust_in_pair,"nclust_in_pair/I"); // number of clusters in pair (1 or 2)
  pairtr->Branch("npoints_in_pair",&npoints_in_pair,"npoints_in_pair/I"); // number of points in pair
  pairtr->Branch("en",&en_tr,"en/F"); // pair energy
  pairtr->Branch("pt",&pt_tr,"pt/F"); // pair transverse momentum
  pairtr->Branch("eta",&eta_tr,"eta/F"); // pair pseudorapidity
  pairtr->Branch("phi",&phi_tr,"phi/F"); // pair azimuth
  pairtr->Branch("mass",&mass_tr,"mass/F"); // pair invariant mass
  pairtr->Branch("dgg",&dgg_tr,"dgg/F"); // transverse distance between points of piar (?check this?)
  pairtr->Branch("zgg",&zgg_tr,"zgg/F"); // energy sharing
  pairtr->Branch("x",&x_tr,"x/F"); // x position
  pairtr->Branch("y",&y_tr,"y/F"); // y position
  //pairtr->Branch("cone_rad",&cone_rad_tr,"cone_rad/F"); // isolation cone radius ??
  //pairtr->Branch("cone_en",&cone_en_tr,"cone_en/F"); // isolation cone energy ??
  //pairtr->Branch("cone_enfrac",&cone_enfrac_tr,"cone_enfrac/F"); // isolation cone energy fraction ??
  pairtr->Branch("en_p",en_p_tr,"en_p[2]/F"); // pair point energy
  pairtr->Branch("x_p",x_p_tr,"x_p[2]/F"); // pair point x
  pairtr->Branch("y_p",y_p_tr,"y_p[2]/F"); // pair point y
  pairtr->Branch("dist_from_edge_p",dist_from_edge_p_tr,"dist_from_edge_p[2]/F"); // distance of pair point from edge
  pairtr->Branch("edge_type_p",edge_type_p_tr,"edge_type_p[2]/I"); // edge type of nearby pair point
  pairtr->Branch("det_p",det_p_tr,"det_p[2]/I"); // detector of cluster associated to pair point
  pairtr->Branch("sigmax",sigmax_p_tr,"sigmax[2]/F"); // sigma max of cluster(s)
  pairtr->Branch("sigmin",sigmin_p_tr,"sigmin[2]/F"); // sigma min of cluster(s)
  pairtr->Branch("chi2ndf_onephot",chi2ndf_onephot_p_tr,"chi2ndf_onephot[2]/F"); // chi2/ndf for 1 photon fit
  pairtr->Branch("chi2ndf_twophot",chi2ndf_twophot_p_tr,"chi2ndf_twophot[2]/F"); // chi2/ndf for 2 photon fit
  pairtr->Branch("clust_id",clust_id_p_tr,"clust_id[2]/I"); // cluster id?

  return kStOK;
}

Int_t StFmsAnalysisMaker::Finish(){
  LOG_INFO << Form("Writing and closing %s",mFilename) << endm;
  mFile->Write();
  mFile->Close();
  return kStOK;
}

Int_t StFmsAnalysisMaker::Make(){
  StEvent* event = (StEvent*)GetInputDS("StEvent");
  if(!event) {LOG_ERROR << "StFmsAnalysisMaker::Make did not find StEvent"<<endm; return kStErr;}
  mFmsColl = event->fmsCollection();
  if(!mFmsColl) {LOG_ERROR << "StFmsAnalysisMaker::Make did not find StEvent->FmsCollection"<<endm; return kStErr;}

  //StSPtrVecFmsHit& hits = mFmsColl->hits();
  StSPtrVecFmsCluster& clusters = mFmsColl->clusters();
  //StSPtrVecFmsPoint& points = mFmsColl->points(); 
  vector<StFmsPointPair*>& pairs = mFmsColl->pointPairs();

  nh=mFmsColl->numberOfHits();
  nc=mFmsColl->numberOfClusters();
  np=mFmsColl->numberOfPoints();
  npair=mFmsColl->numberOfPointPairs();

  for(int b=0; b<2; b++) bc[b]=event->bunchCrossingNumber(b);
  evid = event->id();

  evtr->Fill();


  // cluster loop
  for(int i=0; i<nc; i++){
    StFmsCluster * clu=clusters[i];
    
    det_ctr = clu->detectorId();
    llss = mFmsDbMaker->largeSmall(det_ctr);
    category_ctr = clu->category();
    ntow_ctr = clu->nTowers();
    nphot_ctr = clu->nPhotons();
    en_ctr = clu->energy();
    pt_ctr = (clu->fourMomentum()).perp();
    x_ctr = clu->x();
    y_ctr = clu->y();
    sigmax_ctr = clu->sigmaMax();
    sigmin_ctr = clu->sigmaMin();
    chi2ndf_onephot_ctr = clu->chi2Ndf1Photon();
    chi2ndf_twophot_ctr = clu->chi2Ndf2Photon();
    id_ctr = clu->id();

    clustr->Fill();

    if(mPrint>0) 
      LOG_INFO << Form("Cluster %3d E=%6.2f SigMax=%6.3f",i,en_ctr,sigmax_ctr) << endm;
  }; // eo cluster loop



  // point pair loop
  for(int i=0; i<npair; i++) {
    StFmsPointPair* pair=pairs[i];

    StFmsPoint * pp[2];
    StFmsCluster * pc[2];

    for(int q=0; q<2; q++) {
      pp[q] = pair->point(q);
      en_p_tr[q] = pp[q]->energy();
      x_p_tr[q] = pp[q]->x();
      y_p_tr[q] = pp[q]->y();
      dist_from_edge_p_tr[q] = mFmsDbMaker->distanceFromEdge(pp[q],edge_type_p_tr[q]);
    };


    // pair cut
    /*if(!( en_p_tr[0]>=10.0 && en_p_tr[1]>=10.0 && pair->energy()>30.0 && 
        (dist_from_edge_p_tr[0]<-0.51 || edge_type_p_tr[0]==4) &&
        (dist_from_edge_p_tr[1]<-0.51 || edge_type_p_tr[1]==4) && 
        pair->zgg() < 0.7 && 
        mass_tr<0.2) ) { continue; };
    */

    // loop through cluster(s) of point pair
    // n.b. cluster of first point equals cluster of secont point if point pairs are
    // in only one cluster
    for(int q=0; q<2; q++) {
      pc[q] = pp[q]->cluster();
      det_p_tr[q] = pc[q]->detectorId();
      sigmax_p_tr[q] = pc[q]->sigmaMax();
      sigmin_p_tr[q] = pc[q]->sigmaMin();
      chi2ndf_onephot_p_tr[q] = pc[q]->chi2Ndf1Photon();
      chi2ndf_twophot_p_tr[q] = pc[q]->chi2Ndf2Photon();
      clust_id_p_tr[q] = pc[q]->id();
    };


    // make sure pair is not between two different NSTB's 
    // MAY WANT LOOSEN THIS CONSTRAINT --> veto only piars between large & small cells
    //if(!(det_p_tr[0]==det_p_tr[1])) { continue; };

    llss = mFmsDbMaker->largeSmall(det_p_tr[0]);
    nclust_in_pair = (pc[0]==pc[1]) ? 1:2;
    npoints_in_pair = pair->nPoints();
    en_tr = pair->energy();
    pt_tr = pair->pT();
    eta_tr = pair->eta();
    phi_tr = pair->phi();
    mass_tr=pair->mass();
    dgg_tr = pair->dgg();
    zgg_tr = pair->zgg();
    x_tr = pair->x();
    y_tr = pair->y();
    pairtr->Fill();


    if(mPrint>0) 
      LOG_INFO << Form(" Pair %3d E=%6.2f %6.2f SigMax=%6.3f SigMax=%6.3f",
          i,en_p_tr[0],en_p_tr[1],sigmax_p_tr[0],sigmax_p_tr[1]) << endm;
  }; // eo point pair loop
  return kStOK;
}
