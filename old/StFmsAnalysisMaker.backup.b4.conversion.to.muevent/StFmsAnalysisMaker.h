// \class StFmsAnalysisMaker
// \author Akio Ogawa, modified by dilks
//
//   This is analysis for FMS-FPS correlations.
// 
//  $Id: StFmsAnalysisMaker.h,v 1.5 2015/12/08 17:00:03 akio Exp $
//  $Log: StFmsAnalysisMaker.h,v $
//

#ifndef STAR_StFmsAnalysisMaker_HH
#define STAR_StFmsAnalysisMaker_HH

#include "StMaker.h"
#include "StEnumerations.h"


class StFmsDbMaker;
class StFmsCollection;
class StRpsCollection;
class TH2F;

class StFmsAnalysisMaker : public StMaker{
  public: 
    StFmsAnalysisMaker(const Char_t* name="FmsOffQA");
    ~StFmsAnalysisMaker();
    Int_t Init();
    Int_t Make();
    Int_t Finish();

    void setFileName(char* file){mFilename=file;} 
    void setPrint(int v) {mPrint=v;}

  private:
    StFmsDbMaker* mFmsDbMaker;
    StFmsCollection* mFmsColl;

    char* mFilename;
    TFile* mFile;
    int mPrint;

    // tree for events
    TTree * evtr;
    UInt_t bc[2]; // bxing counter
    Int_t evid; // event id
    Int_t nh,nc,np,npair; // number of hits, clusters, points, pairs

    // tree for clusters
    TTree * clustr;
    Int_t det_ctr;
    Int_t category_ctr,ntow_ctr,nphot_ctr;
    Float_t en_ctr,pt_ctr,x_ctr,y_ctr;
    Float_t sigmax_ctr,sigmin_ctr;
    Float_t chi2ndf_onephot_ctr;
    Float_t chi2ndf_twophot_ctr;
    Int_t id_ctr;

    // tree for point pairs
    TTree * pairtr;
    Int_t llss; // 0=large, 1=small
    Int_t nclust_in_pair; // number of clusters in pair
    Int_t npoints_in_pair; // number of points in pair
    Float_t en_tr,pt_tr,eta_tr,phi_tr,mass_tr,dgg_tr,zgg_tr,x_tr,y_tr; // pair kinematics
    Float_t cone_rad_tr,cone_en_tr,cone_enfrac_tr; // cone kinematics
    Float_t en_p_tr[2]; // pair point energy
    Float_t x_p_tr[2]; // pair point x
    Float_t y_p_tr[2]; // pair point y
    Float_t dist_from_edge_p_tr[2]; // distance of pair point from edge
    Int_t edge_type_p_tr[2]; // edge type nearby pair point
    // cluster variables (if one cluster, first entry of array == second)
    Int_t det_p_tr[2]; // detector of cluster associated to pair point
    Float_t sigmax_p_tr[2]; // sigma max of cluster(s)
    Float_t sigmin_p_tr[2]; // sigma min of cluster(s)
    Float_t chi2ndf_onephot_p_tr[2]; // chi2/ndf for 1 photon fit
    Float_t chi2ndf_twophot_p_tr[2]; // chi2/ndf for 2 photon fit
    Int_t clust_id_p_tr[2]; // cluster id?


    virtual const char *GetCVS() const
    {static const char cvs[]="Tag $Name:  $ $Id: StFmsAnalysisMaker.h,v 1.5 2015/12/08 17:00:03 akio Exp $ built " __DATE__ " " __TIME__ ; return cvs;}

    ClassDef(StFmsAnalysisMaker,0);
};

#endif
