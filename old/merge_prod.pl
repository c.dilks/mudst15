#!/usr/bin/perl -w

my $condordir="condor";
my $logdir="log";
my $cwd=$ENV{'PWD'};

my $opt="none";
my $beam="pptrans";
my $hist="fmsan";
if($#ARGV>=0) {$opt=$ARGV[0];}
if($#ARGV>=1) {$beam=$ARGV[1];}
if($#ARGV>=2) {$hist=$ARGV[2];}
print "Option = $opt (none, submit, exe)\n";
print "Beam = $beam (pptrans,pplong2,pAu or pAl)\n";
print "Hist = $hist (fmsfps,dipi0)\n";

@dirs= ("hist_$beam");
%cmdall=();

if(! -e "$condordir"){`mkdir $condordir`;}

$exe="$condordir/histmerge_$beam.exe";
open(EXE,"> $exe");

$nbad=0;
$bad="$condordir/bad_$beam";
open(BAD,"> $bad");

#read good run list
$goodrunfile="goodruns.dat";
if(! -e $goodrunfile){
  print "Cannot find $goodrunfile\n";
  exit;
}
open(IN,"$goodrunfile");
@goodrunslist=<IN>;
close(IN);
%GOODRUN=();
foreach $run (@goodrunslist){    
  $run=~s/\n//;
  my ($thisrun, $trash) = split " ", $run, 2;
  $GOODRUN{"$thisrun"}=1;
  #printf "good run: $thisrun\n";
}

$condor="$condordir/histmerge.$beam.condor";
open(CONDOR,"> $condor");
print(CONDOR "Universe     = vanilla\n");
print(CONDOR "notification = never\n");
print(CONDOR "getenv       = True\n");
print(CONDOR "Requirements = CPU_Experiment == \"star\"\n");
print(CONDOR "+Experiment  = \"star\"\n");
print(CONDOR "+Job_Type    = \"cas\"\n");
print(CONDOR "\n");

foreach $dir (@dirs){
  $cmdall{"$dir"}="/bin/rm $dir/$hist.root\nhadd $dir/$hist.root ";
  $nfile=0;
  $nrun=0;
  $nday=0;
  $nmudst=0;
  %runs=();
  %days=();
  %cmdrun=();
  %cmdday=();
  opendir(DIR,$dir);
  my @files1= grep {/.$hist.root/} readdir DIR;
  my @files2= grep {!/.merged./} @files1;      
  foreach $file (@files2) {		
    $f =  $file;
    $run = substr($f,0,8);
    $day = substr($run,0,5);
    $fsize = -s "$dir/$file";
    $modtime = (stat("$dir/$file"))[9];
    $localt  = localtime($modtime);
    use Time::Piece;
    $m = localtime($modtime)->month;  
    $d = localtime($modtime)->mday;  
    $y = localtime($modtime)->year;  
    print "$day $run $dir/$file size=$fsize utime=$modtime $localt day=$d month=$m year=$y\n";
#	if($y==2015 || $d<14 || $d>15 || $m=~"Dec") {next;}
#	if($y==2015 || $d>10 || $m=~"Dec") {next;}
    if($fsize < 1000){
      print(BAD "$dir/$file size=$fsize\n");
      $nbad++;
      next;
    }
    if(! defined($GOODRUN{"$run"})) {next;}
    if( ! exists $days{"$day"} ) {
      $days{"$day"}=$nday;
      $nday++;
      $outfileday="$dir/${day}_day.$hist.root";
      $cmdday{"$day"}="/bin/rm $outfileday\nhadd $outfileday";
      $cmdall{"$dir"}="$cmdall{\"$dir\"} $outfileday";
    }
    if( ! exists $runs{"$run"} ) {
      $runs{"$run"}=$nrun;
      $nrun++;
      $outfilerun="$dir/${run}_merged.$hist.root";
      $cmdrun{"$run"}="/bin/rm $outfilerun\nhadd $outfilerun";
      $cmdday{"$day"}="$cmdday{\"$day\"} $outfilerun";
      $nmudst++;	    
    }	
    $cmdrun{"$run"}="$cmdrun{\"$run\"} $dir/$file";
    $nfile++;
  }

  while (($day, $command) = each(%cmdday)) {
    $cmdfile="$condordir/histmerge_${dir}_${day}.exe";
    print "Creating $cmdfile\n";
    open(OUT, "> $cmdfile");
    print(OUT "#!/bin/csh\n");
    close(OUT);
    `/bin/chmod +x $cmdfile`;
  }
  while (($run, $command) = each(%cmdrun)) {
    $day=substr($run,0,5);
    $cmdfile="$condordir/histmerge_${dir}_${run}";
    print "Creating $cmdfile\n";
    open(OUT, "> $cmdfile");
    print(OUT "#!/bin/csh\n");
    print(OUT "$command\n");
    close(OUT);
    `/bin/chmod +x $cmdfile`;

    $cmdfile2="$condordir/histmerge_${dir}_${day}.exe";
    open(OUT2, ">> $cmdfile2");
    print(OUT2 "$cmdfile\n");
    close(OUT2);
  }
  while (($day, $command) = each(%cmdday)) {
    $cmdfile="$condordir/histmerge_${dir}_${day}";
    print "Creating $cmdfile\n";
    open(OUT, "> $cmdfile");
    print(OUT "#!/bin/csh\n");
    print(OUT "$command\n");
    close(OUT);
    `/bin/chmod +x $cmdfile`;

    $cmdfile2="$condordir/histmerge_${dir}_${day}.exe";
    open(OUT2, ">> $cmdfile2");
    print(OUT2 "$cmdfile\n");
    close(OUT2);

    print(EXE "$cmdfile2\n");

    $log="$cwd/$logdir/histmerge_${dir}_${day}.log";
    print(CONDOR "Executable = $cmdfile\n");
    print(CONDOR "Log    = $log\n");
    print(CONDOR "Output = $log\n");
    print(CONDOR "Error  = $log\n");
    print(CONDOR "Queue\n\n");    
  }

  $cmdfile="$condordir/histmerge_${dir}_all";
  print "Creating $cmdfile\n";
  open(OUT, "> $cmdfile");
  print(OUT "$cmdall{$dir}\n");
  close(OUT);
  `/bin/chmod +x $cmdfile`;
  print(EXE "$cmdfile\n");

  print "$nfile histo found for $nrun runs from $nday days in $dir\n";
}
close(EXE);
close(BAD);
`/bin/chmod +x $exe`;
close(CONDOR);
print "Created $condor\n";
print "Created $exe\n";
print "Found $nbad bad histos\n";

if($opt eq "submit" ){
  print("Submitting ${condor}\n");
  system("condor_submit ${condor}\n");
  system("running200.pl\n");
}

if($opt eq "exe" ){
  print("Executing ${exe}\n");
  system("${exe}\n");
}

print "aqui\n";
if($opt eq "submit" || $opt eq "2ndpass"){
  foreach $dir (@dirs){
    $c=$cmdall{"$dir"};
    print("$c\n");
    `$c`;
  }
}

if($opt eq "none" ){
  print("\nnothing will run because opt=${opt}\n");
  print("set opt=exe to execute ${exe}\n");
  print("set opt=submit to submit ${condor} to condor\n");
}
  

