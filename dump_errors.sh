#!/bin/bash

pushd log/
mkdir -p dump
rm -r dump
mkdir -p dump
mkdir -p temp


# look for errors in csh stderr stream; these jobs
# are then called "suspects"
grep '.' cshlog*.err | cut -d: -f1 | uniq | \
  sed 's/cshlog//;s/\.err//' > dump/suspects_csh_list

sep="$(printf '[[[%70s]]]' | tr ' ' :)"

# loop through suspects
while read suspect; do
  printf "job $suspect is suspicious\n\n"

  # print csh errors
  printf "   dump CSH errors for job $suspect\n$sep\n" |\
    tee -a dump/csh_errors
  cat cshlog${suspect}.err | tee -a dump/csh_errors
  printf "\n\n\n" | tee -a dump/csh_errors


  # decompress macro stdout & stderr file
  for ext in out err; do
    if [ -e macrolog${suspect}.${ext}.gz ]; then
      printf "DECOMPRESSING MACRO std${ext} FOR ${suspect}\n"
      gzip -d macrolog${suspect}.${ext}.gz
    fi
  done


  # print error code counts from macro stdout
  printf "   dump macro errors for job $suspect\n$sep\n" |\
    tee -a dump/macro_errors
  tail -n100 macrolog${suspect}.out | grep -A3 "Error Codes" |\
    tee -a dump/macro_errors
  printf "\n\n" | tee -a dump/macro_errors


  # dump macro error file
  cat macrolog${suspect}.err | tee -a dump/macro_errors
  printf "\n\n\n" | tee -a dump/macro_errors


  # move macro error file to a tempoarary directory, since 
  # we are preparing for a second-pass look at macro errors;
  # the first pass only catches macro problems which in turn
  # cause csh problems
  mv macrolog${suspect}.err temp/

  clear
done < dump/suspects_csh_list

printf "\n\n$sep\n%30sEND OF FIRST PASS\n$sep\n\n"
printf "NOW DUMPING ALL OTHER MACRO ERRORS\n"


# decompress other macro error files
numgz=$(ls macrolog*.err.gz | wc -l)
echo "$numgz logs are still compressed"
if [ $numgz -gt 0 ]; then
  echo "decompressing them..."
  gzip -d macrolog*.err.gz
fi
printf "\n\n\n\n"


# get list of non-empty macro log files which remain
grep '.' macrolog*.err | cut -d: -f1 | uniq | \
  sed 's/macrolog//;s/\.err//' > dump/suspects_macro_list

while read suspect; do
  printf "   dump macro errors for job $suspect\n$sep\n" |\
    tee -a dump/macro_errors

  if [ -e macrolog${suspect}.out.gz ]; then
    printf "DECOMPRESSING MACRO stdout FOR ${suspect}\n"
    gzip -d macrolog${suspect}.out.gz
  fi
  tail -n100 macrolog${suspect}.out | grep -A3 "Error Codes" |\
    tee -a dump/macro_errors
  printf "\n\n" | tee -a dump/macro_errors

  cat macrolog$suspect.err | tee -a dump/macro_errors
  printf "\n\n\n" | tee -a dump/macro_errors
done < dump/suspects_macro_list


mv temp/*.err ./
ls . > dump/ls_log_dir
popd

printf "\n"
for file in log/dump/*; do
  echo "less $file"
done
