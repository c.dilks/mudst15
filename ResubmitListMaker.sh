#!/bin/bash
# builds list of runs that need to be resubmitted

# - requires [star-submitted list], which is the list
#   file that you submitted before; if you don't have one
#   just feed it an empty text file

# - output is lists/resubmitList.txt

# - previous resubmitList will be backed up to .bak file

###################################################

if [ $# -ne 1 ]; then
  echo "usage: $0 [star-submitted list]"
  exit
fi

listfile="lists/resubmitList.txt"
infofile="${listfile}.info"


# backup old list
touch $listfile
mv -v ${listfile}{,.bak}
touch $infofile
mv -v ${infofile}{,.bak}
echo "---"

# add runs which have segments that did not reach 'terminated' status
grep -v 'ft1' status/runSummary.txt | \
  awk '{print $1" notTerminated"}' >> $infofile
  

# add runs which have missing OFiles, but still claim to have all
# segments at 'terminated' status
grep -v 'fo1' status/runSummary.txt | grep ft1 | \
  awk '{print $1" terminatedButMissingOFiles"}' >> $infofile


# add runs that we tried to submit to the scheduler, but something went wrong
# with submission and there exist no scheduler logs (and very likely no OFiles)
sort -n $1 | awk '{print $1}' > status/listcheckSubmitted.txt
sort -n status/runSummary.txt | awk '{print $1}' > status/listcheckJobsRan.txt
while read r; do 
  if [ -z `grep $r status/listcheckJobsRan.txt` ]; then
    echo "$r noLogFiles" >> $infofile
  fi
done < status/listcheckSubmitted.txt


# add runs for the case where less than 100% of the available muDST files had
# jobs associated to them, i.e., there were some unprocessed muDSTs
grep -v 'fm1' status/runSummary.txt | \
  awk '{print $1" someUnprocessedMuDsts"}' >> $infofile

# add runs which had any jobs which were aborted by condor
grep -v 'fna1' status/runSummary.txt | \
  awk '{print $1" someAbortedJobs"}' >> $infofile


# build list of unique runs which need to be resubmitted
cat $infofile | awk '{print $1}' > ${listfile}.tmp
cat ${listfile}.tmp | sort -n | uniq > $listfile
rm ${listfile}.tmp


# print result to stdout
cat $infofile
echo "---"
#cat $listfile
#echo "---"
echo "`wc -l $listfile | awk '{print $1}'` unique runs to resubmit"

