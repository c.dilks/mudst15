bulding TTrees with FMS and RP data
-----------------------------------

This is a git repository, hosted on gitlab; If you have a gitlab
account, I can add you as a contributer if you want

Download: git clone https://gitlab.com/c.dilks/mudst15.git

My working area is /star/u/dilks/mudst15/, which you can refer to 


mudst15/README.md contains some documentation on how to run the code;
it is set up so that you submit a list of run numbers to the scheduler
using SubmitList.sh, and you will receive what we call OFiles (named
like OFset[...].root). The OFiles contain a tree with FMS and RP
information. At the moment, the FMS information is rather compressed
and is in a format for the PSU root12fms analysis code. On the other
hand, the RP information is expanded and takes up quite a bit of
space, but allows for easy analysis. For the Run15 transverse
polarization data set, my OFiles take up 770 GB total.


Setup:
- first, you will need to make some directories in a place where you
  have substantial disk space. I run my code from my home directory on
  RCAS and use our PWG working space for storage. Here is my setup:
  - on PWG area, I have a directory called "mudst_files" with the
    following important subdirectories:
    - hist_pptrans: where OFiles get produced
    - log: where scheduler log files appear (if you have a lot of
      errors, the files here can get enormous...)
  - I then symlink these directories back to my main mudst15 code
- now you can make a few more subdirectories in mudst15 (which will
  not take up a lot of space):
  - mkdir condor
  - mkdir log_condor
  - mkdir scheduler
  - mkdir status

- now you need to check out some repositories from CVS; they may
  already be in the git repository under StRoot, but it's best to
  check out the most updated versions of everything
  - Afterburner: it's not officially in StEvent framework, but you can
    get it from Rafal's area and copy it to StRoot. You should refer
    to AfterburnerInstructions.txt, but for now all you need to do is:
    - cvs co offline/users/rafal_s/StMuRpsUtil
    - cp -r offline/users/rafal_s/StMuRpsUtil StRoot/
  - FMS stuff (you can probably just rm these directories in your
    version, if you want to use the latest build)
    - cvs co StRoot/StFmsHitMaker
    - cvs co StRoot/StFmsPointMaker
    - cvs co StRoot/StFmsUtil
  - OFile maker: this is not in CVS, but you have the most updated
    version in this git repository; it is in
    StRoot/StSpinPool/StFmsOFileMaker

Compile: cd to mudst15, type 'cons' and hope for the best!

At this point you can follow README.md to generate your own OFiles,
but you may be more interested in how the code works, which is
described in the next section


The Data Set (XML file)
-----------------------

- you can control the data set you want to submit by editting
  template_scheduler.xml; I'm assuming you have your own XML file at
  this point, but this is the one I use for Run15 transverse pp
- note that I use eventsPerHour="14400". I get an average computing
  rate of 8 events per second, so I cut it in half and tell the
  scheduler to estimate a rate of 4 events per second (14400/hour), in
  order for the jobs to be sent to the correct condor queues 
- if you want to use my XML template file, read it carefully and make
  the necessary edits to make it compatible with your setup

  
The Macro
---------

runMudst4.C -- this is the macro which is sent to the scheduler by
SubmitList.sh. It builds the chain of makers and produces an OFile
- there are some important options, which are explained with comments
  in the code; see the section of the code which says "BEGIN SET MAKER
  OPTIONS"
  - In particular, if the option ofileMk->build_evtr is true, the
    OFile will have an easier-to-read tree with the FMS data (but no
    RP data); this will also take up a LOT of disk space...
  - most of the other options are relevant only to the FMS (to be
    documented later...)


StFmsOFileMaker
---------------

- the tree of interest is called 'p_out', and the RP data are stored
  in the branches which start with "RP_". The cxx file lines which
  have "p_out->Branch(...)" document what each branch is.
  - n_tracks is the number of tracks
  - several track-related branches (RP_t_...) are arrays of size
    n_tracks; one array element is associated with one track
  - the trackpoints are filled similarly (RP_p_...), being arrays of
    size n_trackpoints; note there are two possible track points for
    each track
- the RP data are obtained from StRpsTrack and StRpsTrackPoint
  objects, from StMuRpsCollection2 
  - see StRp_Documentation.pdf and AfterburnerInstructions.txt for
    further documentation
  - note that the afterburner is implemented and working, following those
    AfterburnerInstructions.txt from Rafal. You may want to read
    through these directions anyway to make sure things are okay for
    your setup
- everything else is for compressing the FMS data (to be documented
  later...)


Event Cuts
----------

- Now you have RP information, presumably in an OFile, what do you do
  with it? If you want, I can give my C++ class which reads the RP
  data and analyzes the tracks; it is called "RPevent". Here is what it
  does to check for good tracks:

  - looping through the tracks in the event:
    - demand there are 2 track points in the track
      (t_type==rpsGlobal); in other words, both RP vessels 1 and 2
      must be hit (see StRp_Documentation.pdf for RP vessel
      nomenclature)
    - the tracks pass geometry cuts (see below, under Rafal's Geometry
      Cuts)
    - demand 6 silicon planes were hit (each vessel has 4 planes, so
      with 2 vessels for a track, we have 8 planes; requiring 6 of
      them being hit is a good cleanup cut); you can use the variable
      t_planesUsed
    - According to Rafal and Wlodek, if you are looking for RP tracks
      in one side, say west side, it is good to veto RP activity on the
      other (east) side.


Rafal's Geometry Cuts
---------------------
This function will return true if track number 'track_num' passes
Rafal's geometry cuts:

////
Bool_t passGeom(Int_t track_num) {
  const Double_t thetaRpLimits[2][2] = { 
    {-1.5e-3, 5.0e-3},
    { 1.0e-3, 5.5e-3} 
  };
  enum track_angle_type_enum {
        kAngleThetaX, kAngleThetaY, kAngleTheta, kNumberOfAngleTypes };
  return t_thetaRP[track_num][kAngleThetaX] > thetaRpLimits[0][0] &&
           t_thetaRP[track_num][kAngleThetaX] < thetaRpLimits[0][1] &&
           TMath::Abs(t_thetaRP[track_num][kAngleThetaY]) > thetaRpLimits[1][0] &&
           TMath::Abs(t_thetaRP[track_num][kAngleThetaY]) < thetaRpLimits[1][1];
};
////


Other Documents
---------------

StRp_Documentation.pdf - documentation for RP StEvent objects
StRpsTrack and StRpsTrackPoint

AfterburnerInstructions.txt - how to set up the afterburner
(alignment corrections, hot strip masking, etc.) for the RP analysis;
note that the afterburner is not committed to CVS
