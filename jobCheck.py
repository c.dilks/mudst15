# scans through scheduler log files to determine status of all submitted jobs
# - also computes event rate, useful info for the scheduler to know, so that you use the correct queue
# - assumes python version 2.*
# - author: dilks
#
#
# TERMINOLOGY
# - a jobid corresponds to a run
# - each jobid has multiple 'segment jobs' associated to it; these are the individual condor
#   jobs executed by the scheduler
# - each of these job segments has 1 or more muDSTs associated to it
#
# - a job can be in any of the following states:
#   1. unknown -- this is the state prior to submission to sums (but after calling star-submit)
#   2. submitted -- when the scheduler says the job has been submitted to the queue
#   3. executing -- when the job is running
#   4. terminated -- when the job has finished
#   5. aborted -- condor (or the scheduler) aborted the job
#
#
# OUTPUT
# - outputs various files to the 'status' subdirectory:
#   - [status]Jobs.txt -- list of jobs for each of the 4 states mentioned above, with
#     specific columns (search 'OUTPUT COLUMNS' in the code below)
#
#   - jobSummary.txt -- list of runs and how many segment files are in each of the states
#     -  u=unknown  s=submitted  e=executing  t=terminated  N=total
#
#   - muDSTsThatCrashed.txt -- list of muDst files which crashed (i.e., no OFile came out)
#
#   - *DictDump.txt -- dump of nested dictionary data structures:
#      - mudstDict
#        dictionary of runNumbers, each with a dictionary of mudstFileNames
#      - jobDict
#        dictionary of jobIds, each with a dictionary of individual condor jobs ('xjob's)


import os, fnmatch
import subprocess
import re
import json
import datetime

# options
################################
stream = "st_fms"
schedDir = "scheduler/"
ofileDir = "hist_pptrans"
################################


# define dictionaries
mudstDict = {} # for list of files from fileCatalog (use BuildCatalog.sh)
jobDict = {} # for list of jobs, according to current set of scheduler logs


# first look at list of files; this uses the CATALOG file, which must exist
# this will build 'mudstDict'
runnumTmp = 0
if os.path.isfile("CATALOG15.dat"):
  with open("CATALOG15.dat", 'r') as catalogFile:
    for line in catalogFile:

      line = line.strip()
      cols = line.split()
      runnum = int(cols[0])
      mudstFileName = cols[1]
      mudstFileSize = int(cols[2])
      mudstNevents = int(cols[3])

      if runnum != runnumTmp:
        mudstDict[runnum] = {}
        mudstDict[runnum]['totalNfiles'] = 0
        mudstDict[runnum]['totalNfilesSubmitted'] = 0
        mudstDict[runnum]['totalSize'] = 0
        mudstDict[runnum]['totalNevents'] = 0
        runnumTmp = runnum

      mudstDict[runnum][mudstFileName] = {}
      mudstDict[runnum][mudstFileName]['fileSize'] = mudstFileSize
      mudstDict[runnum][mudstFileName]['nEvents'] = mudstNevents
      mudstDict[runnum][mudstFileName]['jobSubmitted'] = 0 # will be set to 1 if scheduler logs exist

      mudstDict[runnum]['totalNfiles'] += 1
      mudstDict[runnum]['totalSize'] += mudstFileSize
      mudstDict[runnum]['totalNevents'] += mudstNevents

else:
  print "ERROR: you must generate CATALOG15.dat using BuildCatalog.sh"
  exit()
catalogFile.close()



# loop over condor report files; there is one for each jobid, and tells us
# which queue each segment of this job was sent to
listSchedFiles = os.listdir(schedDir)
for entry in listSchedFiles:
  if fnmatch.fnmatch(entry,"*.report"):
    jobid = re.sub('^sched|\..*$', '', entry)
    #print entry+"  jobid="+jobid

    jobDict[jobid] = {}
    jobDict[jobid]['nJobs'] = 0
    jobDict[jobid]['nTerminatedJobs'] = 0
    jobDict[jobid]['nSubmittedJobs'] = 0
    jobDict[jobid]['nExecutingJobs'] = 0
    jobDict[jobid]['nUnknownJobs'] = 0
    jobDict[jobid]['nAbortedJobs'] = 0
    jobDict[jobid]['nOFileExists'] = 0

    with open( schedDir+entry, 'r') as reportFile:
      for line in reportFile:
        if "|" in line:
          line = line.strip()
          cols = line.split()
          jobname = cols[1]
          jobnameCols = jobname.split("_")
          jobidCheck = jobnameCols[0]
          if jobidCheck == jobid:
            segnum = jobnameCols[1]
            if int(segnum)<10: segname = "xjob0"+segnum
            else: segname = "xjob"+segnum

            jobDict[jobid][segname] = {} # start sub-tree for each job segment
            jobDict[jobid][segname]['queue'] = cols[3]


    #jobDict[jobid]['reportFile'] = schedDir+entry
# end loop over condor report files


# loop over list files 
# there are 2 types of list files: 
# -1- sched[JOBID].list = list of this JOBID's segment muDSTS + events
# -2- sched[JOBID]_[SEGNUM].list = list of this segments muDSTS + events
# here we loop through the list of type -2- to get the muDSTS names as well
# as the numbers of events, for each job's segment
nJobs = 0
nMudstsTotal = 0
for entry in listSchedFiles:
  if fnmatch.fnmatch(entry,"*_*.list"):
    jobid = re.sub('^sched|_.*$', '', entry)
    segnum = re.sub('^.*_|\..*$', '', entry)
    if int(segnum)<10: segname = "xjob0"+segnum
    else: segname = "xjob"+segnum

    jobDict[jobid][segname]['status'] = "unknown"
    #jobDict[jobid][segname]['timeSubmitted'] = "0"
    #jobDict[jobid][segname]['timeExecuted'] = "0"
    #jobDict[jobid][segname]['timeTerminated'] = "0"
    #jobDict[jobid][segname]['endMessage'] = ""

    nJobs += 1
    jobDict[jobid]['nJobs'] += 1

    jobDict[jobid][segname]['fileMudstList'] = []

    nEvents = 0
    nMudsts = 0

    print "reading "+entry
    with open(schedDir+entry, 'r') as listFile:
      for line in listFile:
        line = line.strip()
        cols = line.split()
        mudstFile = cols[0]
        mudstFile = re.sub('^.*'+stream, stream, mudstFile)

        # get run number
        if "adc" in mudstFile:
          runnumStr = re.sub(stream+"_adc_","",mudstFile)
          jobDict[jobid][segname]['stream'] = stream+"_adc"
        else:
          runnumStr = re.sub(stream+"_","",mudstFile)
          jobDict[jobid][segname]['stream'] = stream
        runnum = int(re.sub("_.*$","",runnumStr))
        jobDict[jobid]['runnum'] = runnum
        

        # add mudst file name
        jobDict[jobid][segname]['fileMudstList'].append(mudstFile)

        # tell mudstDict that there was a job associated to this mudst
        mudstDict[runnum][mudstFile]['jobSubmitted'] = 1
        mudstDict[runnum]['totalNfilesSubmitted'] += 1

        nEvents += int(cols[1])
        nMudsts += 1
        nMudstsTotal += 1

    # end read listFile

    jobDict[jobid][segname]['nEvents'] = nEvents
    jobDict[jobid][segname]['nMudsts'] = nMudsts
    jobDict[jobid][segname]['OFileSize'] = 0 # initialize OFile size at zero
    jobDict[jobid][segname]['OFileExists'] = 0 # will be set to 1 later, if the OFile is found

    #"print entry+"  jobid="+jobid+"  segname="+segname
# end loop over list files


# loop through condor logs, which tell us when a job started, when and if it
# finished, and how long it took
for entry in listSchedFiles:
  if fnmatch.fnmatch(entry,"*.log"):
    jobid = re.sub('^sched|_.*$', '', entry)
    segnum = re.sub('^.*_|\..*$', '', entry)
    if int(segnum)<10: segname = "xjob0"+segnum
    else: segname = "xjob"+segnum
    #print entry+"  jobid="+jobid+"  segname="+segname

    
    # read list file
    # figure out job status, start time, stop time, etc.
    print "reading "+entry
    with open( schedDir+entry, 'r') as logFile:
      for line in logFile:
        line = line.strip()
        if "submitted" in line:
          cols = line.split()
          jobDict[jobid][segname]['timeSubmitted'] = cols[2]+"-"+cols[3]
          status = "submitted"
          jobDict[jobid][segname]['status'] = status

        elif "executing" in line:
          cols = line.split()
          jobDict[jobid][segname]['timeExecuted'] = cols[2]+"-"+cols[3]
          status = "executing"
          jobDict[jobid][segname]['status'] = status

        elif "terminated" in line:
          cols = line.split()
          jobDict[jobid][segname]['timeTerminated'] = cols[2]+"-"+cols[3]
          status = "terminated"
          jobDict[jobid][segname]['status'] = status
          termStatusObtained = 0

        elif jobDict[jobid][segname]['status'] == "terminated" and termStatusObtained==0:
          jobDict[jobid][segname]['endMessage'] = line
          termStatusObtained = 1

        elif "Total Remote Usage" in line:
          cols = line.split()
          duration = re.sub(",","",cols[2])
          jobDict[jobid][segname]['duration'] = duration

          # compute event rate
          durationS = duration.split(":")
          durationSec = int(durationS[0])*3600.0 + int(durationS[1])*60.0 + int(durationS[2])
          jobDict[jobid][segname]['durationSec'] = durationSec
          jobDict[jobid][segname]['eventRate'] = jobDict[jobid][segname]['nEvents'] / durationSec

        elif "abort" in line:
          cols = line.split()
          status = "aborted"
          jobDict[jobid][segname]['status'] = status

          
    # end read list file

# end loop through condor logs



# loop through ofiles
listOfOFiles = os.listdir(ofileDir)
for entry in listOfOFiles:
  if "fmsan" in entry:
    cols = entry.split(".")
    jobid = cols[1].split("_")[0]
    segnum = cols[1].split("_")[1]
    if int(segnum)<10: segname = "xjob0"+segnum
    else: segname = "xjob"+segnum
    #print entry, jobid, segname

    if jobid in jobDict.keys():
      if segname in jobDict[jobid].keys():
        jobDict[jobid][segname]['OFileSize'] = os.path.getsize(ofileDir+"/"+entry) # units are in bytes
        jobDict[jobid][segname]['OFileExists'] = 1




# define output files
unkStatus = open("status/unknownJobs.dat","w")
subStatus = open("status/submittedJobs.dat","w")
exeStatus = open("status/executingJobs.dat","w")
termStatus = open("status/terminatedJobs.dat","w")
abortStatus = open("status/abortedJobs.dat","w")
runSummary = open("status/runSummary.txt","w")
missingOFiles = open("status/muDSTsThatCrashed.txt","w")

nTerminatedJobs = 0
nSubmittedJobs = 0
nExecutingJobs = 0
nUnknownJobs = 0
nAbortedJobs = 0

nAbnormalJobs = 0
nSizeableJobs = 0

nQueueRCF = 0
nQueueShort = 0
nQueueMedium = 0
nQueueLong = 0 
nQueueUnknown = 0

nMudstsSucceeded = 0
nMudstsFailed = 0
nMudstsExecuting = 0
nMudstsAborted = 0

aveDuration = 0
maxDuration = 0
maxDurationEvRate = 0

aveEventRate = 0
minEventRate = 1000000
maxEventRate = 0



# loop through jobDict ======================================
for jobid, jobidDict in jobDict.iteritems():
  print jobid
  runnum = jobidDict['runnum']
  for key, value in jobidDict.iteritems():
    if "xjob" in key:
      segname = key
      segnum = int(re.sub('xjob','',segname))
      jobname = jobid+"_"+str(segnum)
      #print "  tabulate "+segname

      stream = jobDict[jobid][segname]['stream']
      nEvents = jobDict[jobid][segname]['nEvents']

      if jobDict[jobid][segname]['status'] == "unknown":
        nUnknownJobs += 1
        jobDict[jobid]['nUnknownJobs'] += 1
        nMudstsExecuting += jobDict[jobid][segname]['nMudsts']
        # OUTPUT COLUMNS OF unknown status jobs list
        print >>unkStatus, runnum, jobname, stream, nEvents

      elif jobDict[jobid][segname]['status'] == "aborted":
        nAbortedJobs += 1
        jobDict[jobid]['nAbortedJobs'] += 1
        nMudstsAborted += jobDict[jobid][segname]['nMudsts']
        # OUTPUT COLUMNS OF aborted status jobs list
        print >>abortStatus, runnum, jobname, stream, nEvents


      elif jobDict[jobid][segname]['status'] == "submitted":
        nSubmittedJobs += 1
        jobDict[jobid]['nSubmittedJobs'] += 1
        timeSubmitted = jobDict[jobid][segname]['timeSubmitted']
        nMudstsExecuting += jobDict[jobid][segname]['nMudsts']
        # OUTPUT COLUMNS OF submitted status jobs list
        print >>subStatus, runnum, jobname, stream, nEvents, timeSubmitted

      elif jobDict[jobid][segname]['status'] == "executing":
        nExecutingJobs += 1
        jobDict[jobid]['nExecutingJobs'] += 1
        timeSubmitted = jobDict[jobid][segname]['timeSubmitted']
        timeExecuted = jobDict[jobid][segname]['timeExecuted']
        nMudstsExecuting += jobDict[jobid][segname]['nMudsts']
        # OUTPUT COLUMNS OF executing status jobs list
        print >>exeStatus, runnum, jobname, stream, nEvents, timeSubmitted, timeExecuted

      elif jobDict[jobid][segname]['status'] == "terminated":
        nTerminatedJobs += 1
        jobDict[jobid]['nTerminatedJobs'] += 1
        duration = jobDict[jobid][segname]['duration']
        durationSec = jobDict[jobid][segname]['durationSec']
        eventRate = jobDict[jobid][segname]['eventRate']
        eventSize = jobDict[jobid][segname]['OFileSize'] / jobDict[jobid][segname]['nEvents']
        jobDict[jobid][segname]['eventSize'] = eventSize

        aveDuration += durationSec

        if durationSec>maxDuration: 
          maxDuration = durationSec
          maxDurationEvRate = eventRate

        # care about event rate iff job was at least 3 min long ('sizeable')
        if durationSec>180:
          nSizeableJobs += 1
          if eventRate>maxEventRate: maxEventRate = eventRate
          if eventRate<minEventRate: minEventRate = eventRate
          aveEventRate += eventRate
          

        # check if termination message was abnormal
        endMessage = jobDict[jobid][segname]['endMessage']
        if "Normal termination" in endMessage:
          abnormalTermination = 0
        else:
          abnormalTermination = 1
          nAbnormalJobs += 1

        OFileExists = jobDict[jobid][segname]['OFileExists']
        if OFileExists == 1:
          jobDict[jobid]['nOFileExists'] += 1
          nMudstsSucceeded += jobDict[jobid][segname]['nMudsts']
        else:
          nMudstsFailed += jobDict[jobid][segname]['nMudsts']
          for muFile in jobDict[jobid][segname]['fileMudstList']:
            print >>missingOFiles, runnum, jobid, segname, muFile


        # OUTPUT COLUMNS OF terminated status jobs list
        print >>termStatus, runnum, jobname, stream, nEvents, duration, \
        eventRate, abnormalTermination, OFileExists

      # end if job status==terminated

      # increment counters for each queue
      queue = jobDict[jobid][segname]['queue']
      if queue == "bnl_condor_RCF_quick": nQueueRCF +=1
      elif queue == "bnl_condor_short_quick": nQueueShort +=1
      elif queue == "BNL_condor_medium_quick": nQueueMedium += 1
      elif queue == "bnl_condor_long_quick": nQueueLong += 1
      else: nQueueUnknown += 1

  # end loop through jobidDict
  

  # OUTPUT COLUMNS of job summary list
  #
  # [runnum] [jobid] [# unknownStatus segs] [# submitted] \
  #      [# executing] [# terminated] [total # segs] \
  #      fm[fraction of (m)udst files with jobs associated to them] \
  #      ft[fraction of (t)erminated jobs] \
  #      fo[fraction with existing (O)File] \
  #      fna[fraction of jobs which did not get aborted by condor]
  #
  summaryStr = "u" + str(jobDict[jobid]['nUnknownJobs']) + \
  " s" + str(jobDict[jobid]['nSubmittedJobs']) + \
  " e" + str(jobDict[jobid]['nExecutingJobs']) + \
  " t" + str(jobDict[jobid]['nTerminatedJobs']) + \
  " N" + str(jobDict[jobid]['nJobs']) + \
  " fm" + str( float(mudstDict[runnum]['totalNfilesSubmitted']) / float(mudstDict[runnum]['totalNfiles']) ) + \
  " ft" + str( float(jobDict[jobid]['nTerminatedJobs']) / float(jobDict[jobid]['nJobs']) ) + \
  " fo" + str( float(jobDict[jobid]['nOFileExists']) / float(jobDict[jobid]['nJobs']) ) + \
  " fna" + str( float(jobDict[jobid]['nJobs'] - jobDict[jobid]['nAbortedJobs']) / float(jobDict[jobid]['nJobs']) )

  print >>runSummary, runnum, jobid, summaryStr 
# end loop through jobDict

  
# close output files
unkStatus.close()
abortStatus.close()
subStatus.close()
exeStatus.close()
termStatus.close()
runSummary.close()
missingOFiles.close()


# sort run summary file
os.system("mv status/runSummary.txt{,.x}")
os.system("sort -n status/runSummary.txt.x > status/runSummary.txt")
os.system("rm status/runSummary.txt.x")

# sort run list of muDSTs that crashed
os.system("mv status/muDSTsThatCrashed.txt{,.x}")
os.system("sort -n status/muDSTsThatCrashed.txt.x > status/muDSTsThatCrashed.txt")
os.system("rm status/muDSTsThatCrashed.txt.x")

# print some statistics
if nTerminatedJobs>0: aveDuration /= nTerminatedJobs
if nSizeableJobs>0: aveEventRate /= nSizeableJobs

aveDurationStr = str(datetime.timedelta(seconds=aveDuration))
maxDurationStr = str(datetime.timedelta(seconds=maxDuration))

successPercent = 100*float(nMudstsSucceeded)/float(nMudstsTotal)
failPercent = 100*float(nMudstsFailed)/float(nMudstsTotal)
executePercent = 100*float(nMudstsExecuting)/float(nMudstsTotal)
abortPercent = 100*float(nMudstsAborted)/float(nMudstsTotal)

print "---------------------------------------------"
print "total number of jobs: "+str(nJobs)
print "     not submitted: "+str(nUnknownJobs)
print "         submitted: "+str(nSubmittedJobs)
print "         executing: "+str(nExecutingJobs)
print "          finished: "+str(nTerminatedJobs)
print " aborted by condor: "+str(nAbortedJobs)
print "---------------------------------------------"
print "number of jobs with abnormal termination: "+str(nAbnormalJobs)
print "---------------------------------------------"
print "number of jobs in RCF queue: "+str(nQueueRCF)
print "number of jobs in Short queue: "+str(nQueueShort)
print "number of jobs in Medium queue: "+str(nQueueMedium)
print "number of jobs in Long queue: "+str(nQueueLong)
print "number of jobs in Unrecognized queue: "+str(nQueueUnknown)
print "---------------------------------------------"
print "     total number of muDsts asked for: "+str(nMudstsTotal)
print "                     number successes: "+str(nMudstsSucceeded)+" ("+str(successPercent)+"%)"
print " num. failures (terminated, no ofile): "+str(nMudstsFailed)+" ("+str(failPercent)+"%)"
print "        number executing or submitted: "+str(nMudstsExecuting)+" ("+str(executePercent)+"%)"
print "                       number aborted: "+str(nMudstsAborted)+" ("+str(abortPercent)+"%)"
print "---------------------------------------------"
print "average job runtime: "+aveDurationStr
#print "                     "+str(aveDuration)+" seconds"
print "longest job runtime: "+maxDurationStr+"  (eventRate="+str(maxDurationEvRate)+")"
print "average event rate: "+str(aveEventRate)+" ev/sec (includes only duration>3min jobs)"
print "      slowest rate: "+str(minEventRate)+" ev/sec"
print "      fastest rate: "+str(maxEventRate)+" ev/sec"
print "---------------------------------------------"


# dump mudstDict
mudstDictDump = open("status/mudstDictDump.txt","w")
print >>mudstDictDump, json.dumps(mudstDict,indent=4,sort_keys=True)
mudstDictDump.close()


# dump jobDict
jobDictDump = open("status/jobDictDump.txt","w")
print >>jobDictDump, json.dumps(jobDict,indent=4,sort_keys=True)
jobDictDump.close()
