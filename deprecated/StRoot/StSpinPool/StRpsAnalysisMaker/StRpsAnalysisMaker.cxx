// Based on Rafal Sikora's analysis maker
// Based on the MuDST tools written by Frank Laue.
// Based on the DST Tutorial by Dan Magestro on the STAR Computing/Tutorials page.
// Updated 9/4/2006 by Jim Thomas to include the latest DST format, and Scheduler techniques.

#include "StRpsAnalysisMaker.h"

#include <iostream>

#include "StMuDSTMaker/COMMON/StMuDstMaker.h"
#include "StMuDSTMaker/COMMON/StMuTrack.h"
#include "StMuDSTMaker/COMMON/StMuEvent.h"
#include "StMuDSTMaker/COMMON/StMuRpsCollection.h"
#include "StMuDSTMaker/COMMON/StMuRpsTrackPoint.h"
#include "StMuDSTMaker/COMMON/StMuRpsTrack.h"
#include "StMuDSTMaker/COMMON/StMuTofHit.h"

#include "TH1.h"
#include "TH2.h"
#include "TFile.h"
#include "TObjArray.h"
#include "TList.h"
#include "TTree.h"

ClassImp(StRpsAnalysisMaker)


StRpsAnalysisMaker::StRpsAnalysisMaker( StMuDstMaker* maker ) : StMaker("StRpsAnalysisMaker") {
  mMuDstMaker = maker;
  fTFile = nullptr;
  mEventsProcessed = 0;
  mHistogramOutputFileName = "";
}


StRpsAnalysisMaker::~StRpsAnalysisMaker() {}


Int_t StRpsAnalysisMaker::Init( ) {
  fTFile = new TFile(mHistogramOutputFileName,"RECREATE");

  const char RP[8][5] = { "E1U", "E1D", "E2U", "E2D", "W1U", "W1D", "W2U", "W2D" }; // 2015
  const char branch[4][3] = {"EU", "ED", "WU", "WD"};


  // analysis tree
  rptr = new TTree("rptr","rptr");

  rptr->Branch("bc",bc,"bc[2]/i"); // bunch crossing counter 
  rptr->Branch("evid",&evid,"evid/I"); // event id
  rptr->Branch("n_tracks",&n_tracks,"n_tracks/I");
  rptr->Branch("n_trackpoints",&n_trackpoints,"n_trackpoints/I");

  // tracks
  rptr->Branch("t_index",t_index,"t_index[n_tracks]/I"); // track index number
  rptr->Branch("t_branch",t_branch,"t_branch[n_tracks]/I"); // RP branch (0=EU 1=ED 2=WU 3=WD)
  rptr->Branch("t_type",t_type,"t_type[n_tracks]/I"); // track type (see next line)
    /* 0=rpsLocal -- 1 track point
     * 1=rpsGlobal -- 2 track points
     * 2=rpsUndefined -- track not defined
     */
  rptr->Branch("t_planesUsed",t_planesUsed,"t_planesUsed[n_tracks]/I"); // number of SSD planes hit by track points in track
  rptr->Branch("t_p",t_p,"t_p[n_tracks]/D"); // momentum
  rptr->Branch("t_pt",t_pt,"t_pt[n_tracks]/D"); // transverse momentum
  rptr->Branch("t_eta",t_eta,"t_eta[n_tracks]/D"); // pseudorapidity
  rptr->Branch("t_time",t_time,"t_time[n_tracks]/D"); // time of track detection
  rptr->Branch("t_theta",t_theta,"t_theta[n_tracks]/D"); // polar angle at RP according to STAR coord sys
  rptr->Branch("t_thetaRP",t_thetaRP,"t_thetaRP[n_tracks]/D"); // polar angle at RP according to STAR survey
  rptr->Branch("t_phi",t_phi,"t_phi[n_tracks]/D"); // azimuth at RP according to STAR coord sys
  rptr->Branch("t_phiRP",t_phiRP,"t_phiRP[n_tracks]/D"); // azimuth at RP according to STAR survey
  rptr->Branch("t_t",t_t,"t_t[n_tracks]/D"); // squared 4-momentum transfer
  rptr->Branch("t_xi",t_xi,"t_xi[n_tracks]/D"); // fractional momentum loss (pbeam-p)/pbeam
  rptr->Branch("t_gold",t_gold,"t_gold[n_tracks]/O"); // my track quality variable (2 track points in all 2x4=8 Si planes)

  // track point 0
  rptr->Branch("p0_tpExists",p_tpExists[0],"p0_tpExists[n_tracks]/O"); // true if track point 0 exists
  rptr->Branch("p0_RPid",p_RPid[0],"p0_RPid[n_tracks]/I"); // RP id (see next line)
    /* 0=E1U  1=E1D  2=E2U  3=E2D
     * 4=W1U  5=W1D  6=W2U  7=W2D
     */
  rptr->Branch("p0_quality",p_quality[0],"p0_quality[n_tracks]/I"); // track point quality (see next line)
    /* 0=rpsNormal -- not golden and not undefined
     * 1=rpsGolden -- single cluster in all 4 SSD planes
     * 2=rpsNotSet -- undefined track point
     */
  rptr->Branch("p0_x",p_x[0],"p0_x[n_tracks]/D"); // STAR survey coords x-position
  rptr->Branch("p0_y",p_y[0],"p0_y[n_tracks]/D"); // STAR survey coords y-position
  rptr->Branch("p0_z",p_z[0],"p0_z[n_tracks]/D"); // STAR survey coords z-position
  
  // track point 1
  rptr->Branch("p1_tpExists",p_tpExists[1],"p1_tpExists[n_tracks]/O"); // true if track point 1 exists
  rptr->Branch("p1_RPid",p_RPid[1],"p1_RPid[n_tracks]/I");
  rptr->Branch("p1_quality",p_quality[1],"p1_quality[n_tracks]/I");
  rptr->Branch("p1_x",p_x[1],"p1_x[n_tracks]/D");
  rptr->Branch("p1_y",p_y[1],"p1_y[n_tracks]/D");
  rptr->Branch("p1_z",p_z[1],"p1_z[n_tracks]/D");

  // histograms
  hNumberOfTrackPoints = new TH1F("NumberOfTrackPoints", "NumberOfTrackPoints" , 1001, -0.5, 1000.5);
  hNumberOfTracks = new TH1F("NumberOfTracks", "NumberOfTracks" , 1001, -0.5, 1000.5);


  return kStOK;
}


Int_t StRpsAnalysisMaker::Make() {
  if(debug) LOG_INFO << "[+] calling Make" << endm;

  // open rps collection and run / event info
  StMuEvent* muEvent = mMuDstMaker->muDst()->event();
  if(!muEvent) {
    LOG_ERROR << "StRpsAnalysisMaker::Make did not find StMuEvent" << endm;
    return kStErr;
  };
  if(debug) LOG_INFO << "[+] event opened" << endm;


  StRunInfo runInfo = muEvent->runInfo();
  StMuRpsCollection* muRpsColl = mMuDstMaker->muDst()->RpsCollection();
  if(!muRpsColl) {
    LOG_ERROR << "StRpsAnalysisMaker::Make did not find StRpsColl" << endm;
    return kStErr;
  };
  if(debug) LOG_INFO << "[+] collection opened" << endm;

  //StMuTofHit *tofHit = mMuDstMaker->muDst()->tofHit(0);
  //if(tofHit) LOG_INFO << tofHit->adc() << endm;


  // get event id and number of tracks/trackpoints
  n_trackpoints = 0;
  n_tracks = 0;
  while(muRpsColl->trackPoint(n_trackpoints)) ++n_trackpoints;
  while(muRpsColl->track(n_tracks)) ++n_tracks;
  evid = muEvent->eventId();
  for(int b=0; b<2; b++) bc[b]=(muEvent->eventInfo()).bunchCrossingNumber(b);

  if(debug) LOG_INFO << "[+] evid obtained (" << evid << 
                                "," << n_tracks <<
                                "," << n_trackpoints <<
                                ")" << endm;

  hNumberOfTracks->Fill(n_tracks);
  hNumberOfTrackPoints->Fill(n_trackpoints);

  if(debug) LOG_INFO << "[+] hists filled" << endm;

  // reset track and track point variables
  for(int i=0; i<n_tracks_max; i++) {
    if(debug) LOG_INFO << i << endm;
    t_index[i]=-1;
    t_branch[i]=-1;
    t_type[i]=-1;
    t_planesUsed[i]=-1;
    t_p[i]=-1;
    t_pt[i]=-1;
    t_eta[i]=-100;
    t_time[i]=-1;
    t_theta[i]=-1;
    t_thetaRP[i]=-1;
    t_phi[i]=-100;
    t_phiRP[i]=-100;
    t_t[i]=-1000;
    t_xi[i]=-1000;
    t_gold[i]=false;
    for(int j=0; j<2; j++) {
      if(debug) LOG_INFO << i <<","<<j<< endm;
      p_RPid[j][i]=-1;
      p_quality[j][i]=-1;
      p_x[j][i]=-1000;
      p_y[j][i]=-1000;
      p_z[j][i]=-1000;
    };
  };

  if(debug) LOG_INFO << "[+] branch reset" << endm;

  // track loop
  if(n_tracks>n_tracks_max) {
    LOG_INFO << "WARNING WARNING WARNING -- n_tracks > n_tracks_max; only analysing first " << n_tracks_max << " tracks!" << endm;
    n_tracks = n_tracks_max;
  }
  if(n_tracks>0) {
    for(int i = 0; i<n_tracks; i++) {
      if(debug) LOG_INFO << "for i="<<i<<endm;
      StMuRpsTrack *trk = muRpsColl->track(i);
      
      // fill track leaves
      t_index[i] = i;
      t_branch[i] = trk->branch();
      t_type[i] = trk->type();
      t_planesUsed[i] = trk->planesUsed();
      t_p[i] = trk->p();
      t_pt[i] = trk->pt();
      t_eta[i] = trk->eta();
      t_time[i] = trk->time();
      t_theta[i] = trk->theta(); // no argument returns theta (i.e. not theta_x, etc.)
      t_thetaRP[i] = trk->thetaRp(); // ""  ""
      t_phi[i] = trk->phi();
      t_phiRP[i] = trk->phiRp();
      t_t[i] = trk->t( runInfo.beamEnergy(trk->branch()<2 ? StBeamDirection::east : StBeamDirection::west) );
      t_xi[i] = trk->xi( runInfo.beamEnergy(trk->branch()<2 ? StBeamDirection::east : StBeamDirection::west) );

      // fill track point leaves
      for(int j=0; j<2; j++) {
        if(trk->trackPoint(j)!=NULL) {
          p_tpExists[j][i] = true;
          p_RPid[j][i] = trk->trackPoint(j)->rpId();
          p_quality[j][i] = trk->trackPoint(j)->quality();
          p_x[j][i] = trk->trackPoint(j)->x();
          p_y[j][i] = trk->trackPoint(j)->y();
          p_z[j][i] = trk->trackPoint(j)->z();
        }
        else {
          p_tpExists[j][i] = false;
          p_RPid[j][i] = -1;
          p_quality[j][i] = -1;
          p_x[j][i] = -1000;
          p_y[j][i] = -1000;
          p_z[j][i] = -1000;
        };
      };
      // my track quality cut
      if(t_type[i]==StMuRpsTrack::rpsGlobal && 
         p_quality[0][i]==StMuRpsTrackPoint::rpsGolden &&
         p_quality[1][i]==StMuRpsTrackPoint::rpsGolden) t_gold[i] = true;
      else t_gold[i] = false;
    };


    rptr->Fill(); // fill tree for each event (with at least 1 track)
  };

  if(debug) LOG_INFO << "[+] tree filled" << endm;

  mEventsProcessed++ ;
  return kStOK ;
}


Int_t StRpsAnalysisMaker::Finish() {
  fTFile->Write();
  LOG_INFO << "Total Events Processed in DstMaker " << mEventsProcessed << endm;
  return kStOK;
}









