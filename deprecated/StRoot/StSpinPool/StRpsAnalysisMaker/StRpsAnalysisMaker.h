// Based on Rafal Sikora's analysis maker
// Based on the MuDST tools written by Frank Laue.
// Based on the DST Tutorial by Dan Magestro on the STAR Computing/Tutorials page.
// Updated 9/4/2006 by Jim Thomas to include the latest DST format, and Scheduler techniques.

#ifndef StRpsAnalysisMaker_def
#define StRpsAnalysisMaker_def

#include "StMaker.h"
#include "TString.h"

class StMuDstMaker;
class TFile;
class TH1F;
class TH2F;

const int n_tracks_max = 1000;
const bool debug=false;

class StRpsAnalysisMaker : public StMaker {

  public:

    StRpsAnalysisMaker(StMuDstMaker* maker);
    virtual ~StRpsAnalysisMaker();

    Int_t Init();
    Int_t Make();
    Int_t Finish();

    void SetOutputFileName(TString name) { mHistogramOutputFileName = name; };
    static int NTracksMax() { return n_tracks_max; };


  private:

    StMuDstMaker * mMuDstMaker;
    TFile * fTFile;
    UInt_t mEventsProcessed; //  Number of Events read and processed
    TString mHistogramOutputFileName; //  Name of the histogram output file 

    TTree * rptr;
    UInt_t bc[2]; // bxing counter
    Int_t evid,n_tracks,n_trackpoints;

    // prefix track variables with t_
    Int_t t_index[n_tracks_max];
    Int_t t_branch[n_tracks_max];
    Int_t t_type[n_tracks_max];
    Int_t t_planesUsed[n_tracks_max];
    Double_t t_p[n_tracks_max];
    Double_t t_pt[n_tracks_max];
    Double_t t_eta[n_tracks_max];
    Double_t t_time[n_tracks_max];
    Double_t t_theta[n_tracks_max];
    Double_t t_thetaRP[n_tracks_max];
    Double_t t_phi[n_tracks_max];
    Double_t t_phiRP[n_tracks_max];
    Double_t t_t[n_tracks_max];
    Double_t t_xi[n_tracks_max];
    Bool_t t_gold[n_tracks_max];

    // prefix trackpoint variables with p0_ and p1_
    Bool_t p_tpExists[2][n_tracks_max];
    Int_t p_RPid[2][n_tracks_max];
    Int_t p_quality[2][n_tracks_max];
    Double_t p_x[2][n_tracks_max];
    Double_t p_y[2][n_tracks_max];
    Double_t p_z[2][n_tracks_max];


    // histograms
    TH1F * hNumberOfTrackPoints;
    TH1F * hNumberOfTracks;

    ClassDef(StRpsAnalysisMaker,1)
};

#endif
