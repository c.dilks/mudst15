// \class StFmsOfflineQaMaker
// \author Akio Ogawa
//
//  $Id: StFmsOfflineQaMaker.cxx,v 1.6 2015/12/08 17:00:03 akio Exp $
//

#include "StFmsOfflineQaMaker.h"

#include "StMessMgr.h"
#include "Stypes.h"

#include "StFmsDbMaker/StFmsDbMaker.h"
#include "StEnumerations.h"
#include "StEventTypes.h"
#include "StEvent/StEvent.h"
#include "StEvent/StFmsCollection.h"
#include "StEvent/StFmsHit.h"
#include "StEvent/StFmsPoint.h"
#include "StEvent/StFmsPointPair.h"
#include "StMuDSTMaker/COMMON/StMuTypes.hh"

#include "TFile.h"
#include "TH1F.h"
#include "TTree.h"
#include "TH2F.h"

ClassImp(StFmsOfflineQaMaker);

StFmsOfflineQaMaker::StFmsOfflineQaMaker(const Char_t* name):
  StMaker(name),mFilename((char *)"fmsOffQa.root"),mPrint(1)
{}

StFmsOfflineQaMaker::~StFmsOfflineQaMaker(){}

Int_t StFmsOfflineQaMaker::Init(){  
  mFmsDbMaker=static_cast<StFmsDbMaker*>(GetMaker("fmsDb"));  
  if(!mFmsDbMaker){
    LOG_ERROR  << "StFmsOfflineQaMaker::InitRun Failed to get StFmsDbMaker" << endm;
    return kStFatal;
  }    
  mFile=new TFile(mFilename,"RECREATE");

  mChi2vs[0][0]= new TH2F("Chi1vsChi2L","Chi1vsChi2L",50,0.0,25.0,50,0.0,25.0);
  mChi2vs[1][0]= new TH2F("Chi1vsChi2S","Chi1vsChi2S",50,0.0,25.0,50,0.0,25.0);
  mChi2vs[0][1]= new TH2F("Chi1vsChi2LCut","Chi1vsChi2LCut",50,0.0,25.0,50,0.0,25.0);
  mChi2vs[1][1]= new TH2F("Chi1vsChi2SCut","Chi1vsChi2SCut",50,0.0,25.0,50,0.0,25.0);
  mChi2diff[0][0] = new TH1F("Chi1-Chi2L","Chi1-Chi2L",50,-25.0,25.0);
  mChi2diff[1][0] = new TH1F("Chi1-Chi2S","Chi1-Chi2S",50,-25.0,25.0);
  mChi2diff[0][1] = new TH1F("Chi1-Chi2LCut","Chi1-Chi2LCut",50,-25.0,25.0);
  mChi2diff[1][1] = new TH1F("Chi1-Chi2SCut","Chi1-Chi2SCut",50,-25.0,25.0);

  mSigmaxE[0][0]= new TH2F("SigMaxEL","SigMaxEL",50,0.0,100.0,50,0.0,2.0);
  mSigmaxE[1][0]= new TH2F("SigMaxES","SigMaxES",50,0.0,100.0,50,0.0,2.0);
  mSigmaxE[0][1]= new TH2F("SigMaxELCut","SigMaxELCut",50,0.0,100.0,50,0.0,2.0);
  mSigmaxE[1][1]= new TH2F("SigMaxESCut","SigMaxESCut",50,0.0,100.0,50,0.0,2.0);
  mSigmax[0][0] = new TH1F("SigMaxL","SigMaxL",50,0.0,1.5);
  mSigmax[1][0] = new TH1F("SigMaxS","SigMaxS",50,0.0,1.5);
  mSigmax[0][1] = new TH1F("SigMaxLCut","SigMaxLCut",50,0.0,1.5);
  mSigmax[1][1] = new TH1F("SigMaxSCut","SigMaxSCut",50,0.0,1.5);


  mPtE[0][0] = new TH2F("PtEL","p_{T} vs E -- large cells",100,0,100,100,0,10);
  mPtE[1][0] = new TH2F("PtES","p_{T} vs E -- small cells",100,0,100,100,0,10);
  mPtE[0][1] = new TH2F("PtELCut","p_{T} vs E -- large cells -- cut",100,0,100,100,0,10);
  mPtE[1][1] = new TH2F("PtESCut","p_{T} vs E -- small cells -- cut",100,0,100,100,0,10);


  sigtr = new TTree("sigtr","sigtr");
  //sigtr->Branch("runnum",&runnum,"runnum/I");
  sigtr->Branch("ls",&llss,"ls/I"); // 0=large 1=small
  sigtr->Branch("isPair",&isPair,"isPair/I"); // 0=cluster  1=pair of identical points  2=first point of pair  3=second point of pair
  sigtr->Branch("sigmaMax",&sigm_tr,"sigmaMax/F");
  sigtr->Branch("en",&en_tr,"en/F");
  sigtr->Branch("pt",&pt_tr,"pt/F");
  sigtr->Branch("nph",&nph_tr,"nph/I"); // number of photons in cluster (zero for pairs)
  sigtr->Branch("ntow",&ntow_tr,"ntow/I"); // number of towers in cluster (zero for pairs)
  sigtr->Branch("chisq1",&chisq1,"chisq1/F"); // chi square / ndf of 1-photon fit to cluster (zero for pairs)
  sigtr->Branch("chisq2",&chisq2,"chisq2/F"); // chi square / ndf of 2-photon fit to cluster (zero for pairs)
  sigtr->Branch("en_pair",&en_pair,"en_pair/F"); // energy of pair (zero for clusters)
  sigtr->Branch("mass_pair",&mass_pair,"mass_pair/F"); // mass of pair (zero for clusters)
  sigtr->Branch("z_pair",&z_pair,"z_pair/F"); // Z of pair (zero for clusters)


  return kStOK;
}

Int_t StFmsOfflineQaMaker::Finish(){
  LOG_INFO << Form("Writing and closing %s",mFilename) << endm;
  mFile->Write();
  mFile->Close();
  return kStOK;
}

Int_t StFmsOfflineQaMaker::Make(){
  StEvent* event = (StEvent*)GetInputDS("StEvent");
  if(!event) {LOG_ERROR << "StFmsOfflineQaMaker::Make did not find StEvent"<<endm; return kStErr;}
  mFmsColl = event->fmsCollection();
  if(!mFmsColl) {LOG_ERROR << "StFmsOfflineQaMaker::Make did not find StEvent->FmsCollection"<<endm; return kStErr;}

  //StSPtrVecFmsHit& hits = mFmsColl->hits();
  StSPtrVecFmsCluster& clusters = mFmsColl->clusters();
  //StSPtrVecFmsPoint& points = mFmsColl->points(); 
  vector<StFmsPointPair*>& pairs = mFmsColl->pointPairs();
  //int nh=mFmsColl->numberOfHits();
  int nc=mFmsColl->numberOfClusters();
  //int np=mFmsColl->numberOfPoints();
  int npair=mFmsColl->numberOfPointPairs();

  //loop over clusters
  for(int i=0; i<nc; i++){
    StFmsCluster* clu=clusters[i];
    float e=clu->energy();
    //float pt=sqrt(pow(clu->fourMomentum().px,2)+pow(clu->fourMomentum().py,2));
    float pt=clu->fourMomentum().perp();
    float smax=clu->sigmaMax();
    if(e<30.0) continue;
    int det=clu->detectorId();
    int ls=mFmsDbMaker->largeSmall(det);
    mSigmaxE[ls][0]->Fill(e,smax);
    mSigmax[ls][0]->Fill(smax);
    mChi2vs[ls][0]->Fill(clu->chi2Ndf1Photon(),clu->chi2Ndf2Photon());
    mChi2diff[ls][0]->Fill(clu->chi2Ndf1Photon()-clu->chi2Ndf2Photon());
    mPtE[ls][0]->Fill(e,pt);

    llss = ls;
    isPair = 0;
    sigm_tr = smax;
    en_tr = e;
    pt_tr = pt;
    nph_tr = clu->nPhotons();
    ntow_tr = clu->nTowers();
    chisq1 = clu->chi2Ndf1Photon();
    chisq2 = clu->chi2Ndf2Photon();
    en_pair = 0;
    mass_pair = 0;
    z_pair = 0;
    sigtr->Fill();

    if(mPrint>0) 
      LOG_INFO << Form("Cluster %3d E=%6.2f SigMax=%6.3f",i,e,smax) << endm;
  }

  //Loop over StFmsPointPairs
  for(int i=0; i<npair; i++) {
    StFmsPointPair* pair=pairs[i];
    StFmsPoint* p0=pair->point(0);
    StFmsPoint* p1=pair->point(1);
    float e0=p0->energy();
    float e1=p1->energy();
    float mass=pair->mass();
    int edgeType0, edgeType1;
    float d0=mFmsDbMaker->distanceFromEdge(p0,edgeType0); 
    float d1=mFmsDbMaker->distanceFromEdge(p1,edgeType1); 
    if( e0>=10.0 && e1>=10.0 && pair->energy()>30.0 && 
        (d0<-0.51 || edgeType0==4) &&
        (d1<-0.51 || edgeType1==4) && 
        pair->zgg() < 0.7 &&
        mass<0.2 ){
      StFmsCluster *c0=p0->cluster();
      StFmsCluster *c1=p1->cluster();
      if(c0==c1){
        float ce=c0->energy();
        //float cpt=sqrt(pow(c0->fourMomentum().px,2)+pow(c0->fourMomentum().py,2));
        float cpt=c0->fourMomentum().perp();
        float smax=c0->sigmaMax();
        int det=c0->detectorId();
        int ls=mFmsDbMaker->largeSmall(det);
        mSigmaxE[ls][1]->Fill(ce,smax);
        mSigmax[ls][1]->Fill(smax);
        mChi2vs[ls][1]->Fill(c0->chi2Ndf1Photon(),c0->chi2Ndf2Photon());
        mChi2diff[ls][1]->Fill(c0->chi2Ndf1Photon()-c0->chi2Ndf2Photon());
        mPtE[ls][1]->Fill(ce,cpt);

        llss = ls;
        isPair = 1;
        sigm_tr = smax;
        en_tr = ce;
        pt_tr = cpt;
        nph_tr = 0; // set to 0 for now...
        ntow_tr = 0; // set to 0 for now...
        chisq1 = 0; // set to 0 for now...
        chisq2 = 0; // set to 0 for now...
        en_pair = ce;
        mass_pair = mass;
        z_pair = pair->zgg();
        sigtr->Fill();

        if(mPrint>0) 
          LOG_INFO << Form(" Pair %3d E=%6.2f %6.2f SigMax=%6.3f",
              i,e0,e1,smax) << endm;       
      }else{
        float ec0=c0->energy();
        float ec1=c1->energy();
        //float cpt0=sqrt(pow(c0->fourMomentum().px,2)+pow(c0->fourMomentum().py,2));
        //float cpt1=sqrt(pow(c1->fourMomentum().px,2)+pow(c1->fourMomentum().py,2));
        float cpt0=c0->fourMomentum().perp();
        float cpt1=c1->fourMomentum().perp();
        float smax0=c0->sigmaMax();
        float smax1=c1->sigmaMax();
        int det0=c0->detectorId();
        int ls0=mFmsDbMaker->largeSmall(det0);
        int det1=c1->detectorId();
        int ls1=mFmsDbMaker->largeSmall(det1);
        mSigmaxE[ls0][1]->Fill(ec0,smax0);
        mSigmaxE[ls1][1]->Fill(ec1,smax1);
        mSigmax[ls0][1]->Fill(smax0);
        mSigmax[ls1][1]->Fill(smax1);
        mChi2vs[ls0][1]->Fill(c0->chi2Ndf1Photon(),c0->chi2Ndf2Photon());
        mChi2diff[ls0][1]->Fill(c0->chi2Ndf1Photon()-c0->chi2Ndf2Photon());
        mChi2vs[ls1][1]->Fill(c1->chi2Ndf1Photon(),c1->chi2Ndf2Photon());
        mChi2diff[ls1][1]->Fill(c1->chi2Ndf1Photon()-c1->chi2Ndf2Photon());
        mPtE[ls0][1]->Fill(ec0,cpt0);
        mPtE[ls1][1]->Fill(ec1,cpt1);

        llss = ls0;
        isPair = 2;
        sigm_tr = smax0;
        en_tr = ec0;
        pt_tr = cpt0;
        nph_tr = 0; // set to 0 for now...
        ntow_tr = 0; // set to 0 for now...
        chisq1 = 0; // set to 0 for now...
        chisq2 = 0; // set to 0 for now...
        en_pair = ec0+ec1;
        mass_pair = mass;
        z_pair = pair->zgg();
        sigtr->Fill();

        llss = ls1;
        isPair = 3;
        sigm_tr = smax1;
        en_tr = ec1;
        pt_tr = cpt1;
        nph_tr = 0; // set to 0 for now...
        ntow_tr = 0; // set to 0 for now...
        chisq1 = 0; // set to 0 for now...
        chisq2 = 0; // set to 0 for now...
        en_pair = ec0+ec1;
        mass_pair = mass;
        z_pair = pair->zgg();
        sigtr->Fill();

        if(mPrint>0) 
          LOG_INFO << Form(" Pair %3d E=%6.2f %6.2f SigMax=%6.3f SigMax=%6.3f",
              i,e0,e1,smax0,smax1) << endm;
      }
    }
  }//loop over pairs
  return kStOK;
}
