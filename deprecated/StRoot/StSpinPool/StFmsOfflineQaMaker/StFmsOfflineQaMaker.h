// \class StFmsOfflineQaMaker
// \author Akio Ogawa
//
//   This is analysis for FMS-FPS correlations.
// 
//  $Id: StFmsOfflineQaMaker.h,v 1.5 2015/12/08 17:00:03 akio Exp $
//  $Log: StFmsOfflineQaMaker.h,v $
//

#ifndef STAR_StFmsOfflineQaMaker_HH
#define STAR_StFmsOfflineQaMaker_HH

#include "StMaker.h"
#include "StEnumerations.h"

class StFmsDbMaker;
class StFmsCollection;
class TH2F;

class StFmsOfflineQaMaker : public StMaker{
  public: 
    StFmsOfflineQaMaker(const Char_t* name="FmsOffQA");
    ~StFmsOfflineQaMaker();
    Int_t Init();
    Int_t Make();
    Int_t Finish();

    void setFileName(char* file){mFilename=file;} 
    void setPrint(int v) {mPrint=v;}

  private:
    StFmsDbMaker* mFmsDbMaker;
    StFmsCollection* mFmsColl;

    char* mFilename;
    TFile* mFile;
    int mPrint;

    //cluster related
    TH2F* mChi2vs[2][2]; // [ls] [0=w/o cut; 1=w/cut]
    TH1F* mChi2diff[2][2];
    TH2F* mSigmaxE[2][2];
    TH1F* mSigmax[2][2];

    // point pair related
    TH2F * mPtE[2][2];

    // data tree for SigmaMax
    TTree * sigtr;
    Int_t runnum,llss,isPair;
    Float_t sigm_tr,en_tr,pt_tr;
    Int_t nph_tr,ntow_tr; // of cluster (zero for pairs)
    Float_t chisq1,chisq2; // of cluster (zero for pairs)
    Float_t en_pair,mass_pair,z_pair; // pair of points (zero for clusters)


    virtual const char *GetCVS() const
    {static const char cvs[]="Tag $Name:  $ $Id: StFmsOfflineQaMaker.h,v 1.5 2015/12/08 17:00:03 akio Exp $ built " __DATE__ " " __TIME__ ; return cvs;}

    ClassDef(StFmsOfflineQaMaker,0);
};

#endif
