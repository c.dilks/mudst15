#!/bin/bash
# see if a 'good run' is in the file catalog

catFile="CATALOG15.dat"
goodList="lists/good_trans.dat"

while read line; do
  run=$(echo $line|awk '{print $1}')
  grepRes=$(egrep "^$run" $catFile)
  if [ -z "$grepRes" ]; then
    echo "$run not in catalog"
  fi
done < $goodList
