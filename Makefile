OPTS := $(shell root-config --cflags --glibs)

All: diag

diag: DiagnosticRP.cpp
	g++ DiagnosticRP.cpp -o diag.exe $(OPTS)

clean:
	@rm diag.exe
