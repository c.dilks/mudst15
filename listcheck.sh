#!/bin/bash
# compares a given list (which was submitted to scheduler, such as those in lists/ directory)
# to the current status/runSummary.txt list (generated with `python jobCheck.py`)
#
# this is so that we can have a bonafide way of making sure all runs we wanted to submit 
# were assigned jobs

if [ $# -ne 1 ]; then
  echo "usage: $0 [star-submitted list]"
  exit
fi

sort -n $1 | awk '{print $1}' > status/listcheckSubmitted.txt
sort -n status/runSummary.txt | awk '{print $1}' > status/listcheckJobsRan.txt

vimdiff status/listcheck{Submitted,JobsRan}.txt

while read submittedRun; do 
  if [ -z `grep $submittedRun status/listcheckJobsRan.txt` ]; then
    echo $submittedRun
  fi
done < status/listcheckSubmitted.txt
  

