# computes fraction of muDSTs which crashed
# takes the list of runs you submitted, looks at status/ directory's current
# list of muDSTs that crashed, and determines what fraction of the total number of muDSTs
# requested crashed

import os, fnmatch
import subprocess
import re
import json
import datetime
import sys

if len(sys.argv) < 2:
  print "usage: ",sys.argv[0]," [submitted list] [status dir (default status/)]"
  exit()

submittedList = sys.argv[1]
if len(sys.argv) >= 3:
  statusDir = sys.argv[2]
else:
  statusDir = "status"


# first look at list of files; this uses the CATALOG file, which must exist
# this will build 'mudstDict'
mudstDict = {} # for list of files from fileCatalog (use BuildCatalog.sh)
runnumTmp = 0
if os.path.isfile("CATALOG15.dat"):
  with open("CATALOG15.dat", 'r') as catalogFile:
    for line in catalogFile:

      line = line.strip()
      cols = line.split()
      runnum = int(cols[0])
      mudstFileName = cols[1]
      mudstFileSize = int(cols[2])
      mudstNevents = int(cols[3])

      if runnum != runnumTmp:
        mudstDict[runnum] = {}
        mudstDict[runnum]['totalNfiles'] = 0
        mudstDict[runnum]['totalNfilesSubmitted'] = 0
        mudstDict[runnum]['totalSize'] = 0
        mudstDict[runnum]['totalNevents'] = 0
        runnumTmp = runnum

      mudstDict[runnum][mudstFileName] = {}
      mudstDict[runnum][mudstFileName]['fileSize'] = mudstFileSize
      mudstDict[runnum][mudstFileName]['nEvents'] = mudstNevents
      mudstDict[runnum][mudstFileName]['mudstCrashed'] = 0 # will be set to 1 if the muDST's job(s) crashed

      mudstDict[runnum]['totalNfiles'] += 1
      mudstDict[runnum]['totalSize'] += mudstFileSize
      mudstDict[runnum]['totalNevents'] += mudstNevents

else:
  print "ERROR: you must generate CATALOG15.dat using BuildCatalog.sh"
  exit()
catalogFile.close()


# loop through list of crashed muDSTs and mark them as 'crashed' in mudstDict
crashFile = statusDir+"/muDSTsThatCrashed.txt"
if os.path.isfile(crashFile):
  with open(crashFile,'r') as crashFileObj:
    for line in crashFileObj:
      line = line.strip()
      cols = line.split()
      runnum = int(cols[0])
      muFile = cols[3]
      mudstDict[runnum][muFile]['mudstCrashed'] = 1
else:
  print "error: bad statusDir"
  exit()


# loop through list of submitted runs and count how many crashed and total muDST files there were
nMudst=0
nCrashed=0
nMudstTotal=0
nCrashedTotal=0

if os.path.isfile(submittedList):
  with open(submittedList,'r') as submittedListObj:
    for line in submittedListObj:
      nMudst=0
      nCrashed=0
      line = line.strip()
      cols = line.split()
      runnum = int(cols[0])
      for key, value in mudstDict[runnum].iteritems():
        if "root" in key:
          nMudst += 1
          nMudstTotal += 1
          if value['mudstCrashed'] == 1:
            nCrashed += 1
            nCrashedTotal += 1
      print runnum,nCrashed,nMudst
else:
  print "error: bad submittedList"
  exit()
print "total",nCrashedTotal,nMudstTotal



#print json.dumps(mudstDict,indent=4,sort_keys=True)


# dump mudstDict
#mudstDictDump = open("status/mudstDictDump.txt","w")
#print >>mudstDictDump, json.dumps(mudstDict,indent=4,sort_keys=True)
#mudstDictDump.close()
