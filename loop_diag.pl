#!/usr/bin/perl -w

my $dir="hist_pptrans";
my $log="log_diag";

#my $exe=`which root`;
#chomp($exe);
my $exe="diag.exe";

my $batfile="diag.bat";
open(BAT, "> ${batfile}");
print(BAT "Executable = ${exe}\n");
print(BAT "Universe = vanilla\n");
print(BAT "notification = never\n");
print(BAT "getenv = True\n");
print(BAT "+Experiment = \"star\"\n");
print(BAT "+Job_Type = \"cas\"\n");

my @infiles = <${dir}/OFset*.root>;
foreach $file (@infiles) {
  for (my $ewb=0; $ewb<=2; $ewb++) {
    $fname=$file;
    $fname=~s/^.*OFset/diag/;
    $fname=~s/\.root//;
    print("file=$file  fname=$fname\n");
    #print(BAT "\nArguments = -b -q DiagnosticRP.C+(\\\"${file}\\\",${ewb})\n");
    print(BAT "\nArguments = ${file} ${ewb}\n");
    print(BAT "Log    = ${log}/${fname}.ewb${ewb}.log\n");
    print(BAT "Output = ${log}/${fname}.ewb${ewb}.out\n");
    print(BAT "Error  = ${log}/${fname}.ewb${ewb}.err\n");
    print(BAT "Queue\n");
  }
}
close(BAT);

system("condor_submit ${batfile}"); 
