#!/bin/bash


# clean up log dirs
./cleanup


# backup resubmitList with timestamp (so we don't overwrite it later)
listfile="lists/resubmitList.txt"
timestamp=`date +%j.%T | sed 's/:/./g'`
echo "---"
echo "backup resubmitList:"
cp -v ${listfile}{,.${timestamp}}
listfile="${listfile}.${timestamp}"


# backup ofiles to be resubmitted
ofileDir="hist_pptrans"
mkdir -p ${ofileDir}/resubmitted
echo ""
echo "---"
echo "backup OFiles which will be resubmitted;"
echo "IGNORE any mv: cannot stat errors here:"
echo ""
while read runnum; do
  mv -v ${ofileDir}/${runnum}*.fmsan.root ${ofileDir}/resubmitted/
done < $listfile


# backup status/ dir, so we can compare things like # of events
echo ""
echo "---"
echo "backup jobCheck.py status/ dir"
mkdir -p status.bak
cp -v status/* status.bak/


# call star_submit on the backupped file
echo "---"
echo "NOW SUBMITTING $listfile !!!"
SubmitList.sh $listfile 2>&1 | tee submit.log
