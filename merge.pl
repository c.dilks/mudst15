#!/usr/bin/perl -w
use Data::Dumper;

# main directories
my $cwd=$ENV{'PWD'};
my $condordir="condor";
my $logdir="log_condor";

# arguments
my $opt="none";
my $beam="pptrans";
my $hist="fmsan";
if($#ARGV>=0) {$opt=$ARGV[0];}
if($#ARGV>=1) {$beam=$ARGV[1];}
if($#ARGV>=2) {$hist=$ARGV[2];}
print "Option = $opt (none, condor, exe)\n";
print "Beam = $beam (pptrans,pplong2,pAu or pAl)\n";
print "Hist = $hist (fmsfps,dipi0)\n";


# set up executables and batch file
$unixtime = time;

$exe="${condordir}/exe${unixtime}.exe";
open(EXE,"> $exe");
print(EXE "#!/bin/bash\n");

if(! -e "$condordir"){`mkdir $condordir`;}
$condor="${condordir}/condor${unixtime}.bat";
open(CONDOR,"> $condor");
print(CONDOR "Universe     = vanilla\n");
print(CONDOR "notification = never\n");
print(CONDOR "getenv       = True\n");
print(CONDOR "Requirements = CPU_Experiment == \"star\"\n");
print(CONDOR "+Experiment  = \"star\"\n");
print(CONDOR "+Job_Type    = \"cas\"\n");
print(CONDOR "Executable = /usr/bin/bash\n");
print(CONDOR "\n");


# initialize data structures etc.
my @ofsetlist = (); # list of OFile set names to be output
my @infilelist = (); # organized list of input files to be hadd'ed
                     # -> it's a multidimensional array
                     # [OFile] [run# within day (e.g. 16100023->23)] [segment]
my %idx_of_ofset; # OFile -> index hash
my $idx = 0;  # OFile index


# loop through data directories
@dirs= ("hist_$beam");
foreach $dir (@dirs){
  print("dir=${dir}\n");
  opendir(DIR,$dir);

  # obtain list of rootfiles to be merged
  my @allrootfiles = grep {/.$hist.root/} readdir DIR;
  my @singlerootfiles = grep {!/.OFset./} @allrootfiles;
  @singlerootfiles = sort @singlerootfiles;
  
  
  # build infilelist (organized multi-dimensional array of input files)
  foreach $file (@singlerootfiles) {
    # first some enumeration
    my ($runnum, $jobidfull, $analysis, $filetype) = split('\.', $file);
    my ($jobid, $segment) = split "_", $jobidfull;
    my $setnum = substr($runnum,2,5);
    my $run_within_day = substr($runnum,5,3);
    my $ofsetname = "OFset".$setnum.".root";
    print(" - ${file} --> run${runnum} seg${segment}\n");

    # if it's a new OFset, push it onto the list of OFsets and
    # update the hash table
    if(! (grep $_ eq ${ofsetname}, @ofsetlist)) { 
      push(@ofsetlist,$ofsetname); 
      $idx_of_ofset{$ofsetname} = $idx;
      $idx++;
    }

    # add this file name to its proper place in infilelist
    $infilelist[$idx_of_ofset{$ofsetname}][$run_within_day][$segment] = $file;
  }

  # define reversed hash table for OFset -> its index
  my %ofset_of_idx = reverse %idx_of_ofset;
  @ofsetlist = sort @ofsetlist;


  # dump hash tables and infilelist to stdout
  print("@ofsetlist\n");
  print("HASH TABLE ofset_of_idx\n");
  print Dumper(\%ofset_of_idx);
  print("HASH TABLE idx_of_ofset\n");
  print Dumper(\%idx_of_ofset);
  print("ARRAY infilelist\n");
  print Dumper(\@infilelist);


  # LOOP THROUGH INFILELIST
  # loop over OFiles
  for my $i_ofile (0 .. $#infilelist) {
    $ofilename = $ofset_of_idx{$i_ofile};
    (my $exename = $ofilename) =~ s/root/exe/;
    $logname="${cwd}/${logdir}/${exename}";

    $ofilename = $dir."/".$ofilename;
    $exename = $condordir."/".$exename;

    print "\n$ofilename to be built by $exename\n"; 

    # write out exe script header and start hadd command line
    open(OUT, "> $exename");
    print(OUT "#!/bin/bash\n");
    print(OUT "touch $ofilename\n");
    print(OUT "rm $ofilename\n");
    $haddcmd = "hadd $ofilename";
    $mvcmd = "mv -v";

    # loop over runs in this OFile
    foreach $run (@{$infilelist[$i_ofile]}) {
      # loop over segments in this run
      foreach $ifile (@$run) {
        # add this segment to the hadd command line
        if($ifile) {
          $ifile = $dir."/".$ifile;
          print "hadd $i_ofile $ifile\n";
          $haddcmd = $haddcmd." $ifile";
          $mvcmd = $mvcmd." $ifile";
        }
      }
    }
    $mvcmd = $mvcmd." ${dir}/merged/";
    if(! -e "${dir}/merged"){`mkdir ${dir}/merged`;}
    print(OUT "$haddcmd\n");
    print(OUT "$mvcmd\n");
    close(OUT);

    # add this exe to the batch file
    print(CONDOR "Arguments = $cwd/$exename\n");
    print(CONDOR "Log    = ${logname}.log\n");
    print(CONDOR "Output = ${logname}.out\n");
    print(CONDOR "Error  = ${logname}.err\n");
    print(CONDOR "Queue\n\n");    

    # add this exe to the main exe
    print(EXE "$cwd/$exename\n");

    # make this exe user-executable 
    `/bin/chmod +x $cwd/$exename`;
  }


} # eo dir loop

close(CONDOR);
close(EXE);

# make main exe user-executable
`/bin/chmod +x $exe`;


if($opt eq "exe" ){
  print("Executing ${exe}\n");
  system("${cwd}/${exe}\n");
}

elsif($opt eq "condor" ){
  print("Submitting ${condor}\n");
  system("condor_submit ${condor}\n");
}

else {
  print("\nexecution option unknown or unspecified\n");
  print("the following exe and condor files have been created:\n");
  print("${exe}\n${condor}\n");
  print("\npass \"exe\" or \"condor\" to this script to proceed\n");
}
