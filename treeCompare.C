// compares p_out trees of two trigger files

void treeCompare(
  TString file1 = "st_fms_16088020_raw_5500016.fmsan.root",
  TString file2 = "st_fms_16088020_raw_5500016.fmsan.angleYuxi.root",
  TString treename = "p_out"
) {

  TString filename[2] = {file1,file2};

  TFile * infile[2];
  TTree * tr[2];

  for(int i=0; i<2; i++) {
    infile[i] = new TFile(filename[i].Data(),"READ");
    if(infile[i]==NULL) { fprintf(stderr,"file does not exist\n"); return; };

    tr[i] = (TTree*) infile[i]->Get(treename.Data());
    if(tr[i]==NULL) { fprintf(stderr,"tree does not exist\n"); return; };

    cout << treename << " in "<< filename[i] << " has " << 
            tr[i]->GetEntries() << " entries" << endl;
  };

  tr[0]->AddFriend(tr[1],"tr1");
  //tr[0]->Draw("bc-tr1.bc");
  tr[0]->Draw("br_pxyzt[7]-tr1.br_pxyzt[7]","","colz");
};
 
