TERSE PROCEDURE
---------------
`
- SubmitList.sh [list file] 2>&1 | tee submit.log
  - list file can be any list of runs (only the first column is considered,
    so if your run list is actually a table, no problem as long as first
    column is the run number)

- check scheduler status, and resubmit failed jobs if necessary
  - python jobCheck.py 
    --> look at the summary of how many jobs are running or finished
    --> make sure there are no jobs with 'abnormal termination'
    --> use CatalogMaker.sh to build the CATALOG file, which is the list of 
        available muDSTs
  - ResubmitListMaker.sh
    --> this will make a list of runs which need to be resubmitted
    --> lists/resubmitList.txt.info has runs (not uniq'ed or sorted) and
        a reason why each one is on the list;
    --> lists/resubmitList.txt has the list of unique runs which
        need to be resubmitted
  - SubmitListResubmit.sh
    --> use 'SubmitListResubmit.sh' to resubmit the list of runs to resubmit
        to the scheduler
        - runs 'cleanup' (which will delete scheduler and error logs!)
        - makes backups of the OFiles associated to jobs which will be
          resubmitted, 
        - then call star_submit on the resubmit list (which will also be backed
          up and timestamped, in case you need to re-resubmit)
        - also backs up the status subdir


    Notes on reading jobCheck.py status/ files:

    - if you want to look manually at the logs, the following commands
      may help you (they are used by ResubmitListMaker.sh)
      - grep -v ft1 status/runSummary.txt > resubmitList.txt
        - if the scheduler is fully done, but there are still jobs with
          'status=unknown', this command makes a list of jobs to re-submit 
      - grep -v fo1 status/runSummary.txt
        - checks to see if we got OFiles out the other end (barring any errors
          in the error logs (see below for how to check that) )
      - listcheck.sh
        - compares the list of runs which had scheduler logs to the
          list of runs that you submitted: opens up a vimdiff between
          the lists
      - if you resubmit jobs, make sure your resubmit list generated
        from the above commmands is passed through sort -n | uniq

      - use CountEventsInOFile.C to count the number of events in an OFile
        

- scan for script errors
- dump_errors.sh 
  - grep 'macro.*err' log/dump/ls_log_dir -c
    -- counts total number of MuDSTs processed
  - less log/dump/macro_errors
    -- scan through searching for ":::" to inspect all the errors
  - mv log/dump{,_${SETNUM}}

- merge all the segment files into OFiles
  - merge.pl condor > out_merge_${SETNUM}.out
  - mkdir hist_pptrans/SUPERSET_${SETNUM}
  - mv hist_pptrans/*.root hist_pptrans/SUPERSET_${SETNUM}/

- clean things up
  - rm hist_pptrans/merged/*.root
  - run 'cleanup' to get rid of various files (including logs!)
`



NOTE ON COMPILERS
-----------------
- `gcc 4.8.2` is currently used by `cons` on my node; object files are currently written to `.sl64_gcc482`
- some nodes on RCF use `gcc 4.7.7`; these nodes will look for a `.sl64_gcc477` directory 
  instead of `.sl64_gcc482`; thus I've made a symlink `.sl64_gcc477 -> .sl64_gcc482` to avoid this issue


SUBMITTING A JOB TO SUMS (STAR UNIFIED META SCHEDULER)
------------------------------------------------------
- `SubmitList.sh lists/[LIST]` will use `template_scheduler.xml` to produce and submit an
  `xml` file, by means of `star-submit-template`
  - `star-submit-template` will produce `schedTemplateExp.xml`, which is a temporary
    file on which `star-submit` is called
  - `star-submit` starts the scheduler
- ROOT macro is the chain, and is controlled by the command block of
  `template-scheduler.xml`
  - enviroment / libs are set here
  - output is saved and compressed
  - ROOT macro controls naming of output files
  - copy block of `scheduler/sched*.csh` shell scripts will bring root files back to the
    `output` directory specified in `template-scheduler.xml`; log files must be copied
    manually
  - `Generator` block of `template-scheduler.xml` says where all the shell scripts and
    condor batch files will be stored, along with file lists


FILES PRODUCED DURING JOB SUBMISSION & EXECUTION
-------------------------------------------------
- `scheduler/`
  - files for each segment for each run
      - `sched[JOBID]_[SEG].csh` -- shell script for executing job on a specific file, and
        whatever else is in the `command` block of the `xml` file
      - `sched[JOBID]_[SEG].list` -- this is the `$FILELIST` variable in the aforementioned
        shell script; this is a list of the MuDST(s) to be read
      - `sched[JOBID]_[SEG].condor.log` -- condor job status log
  - files for each run
      - `sched[JOBID]_0_[# SEGs].condor` -- batch file, executing all shell scripts for all
        segments for this run; the job ID is unique for each run
      - `sched[JOBID].list` -- full file list; `$FILELIST_ALL` variable in shell scripts
      - `sched[JOBID].report` -- status of found MuDSTs


- `log/`
  - `cshlog[ID]_[SEG].out` -- `stdout` output of `scheduler/sched*.csh` shell script
  - `cshlog[ID]_[SEG].err` -- `stderr` output of `scheduler/sched*.csh` shell script
  - `macrolog[ID]_SEG.out.gz` -- `stdout` of ROOT macro
  - `macrolog[ID]_SEG.err.gz` -- `stderr` of ROOT macro


- `./`
  - `[JOBID].session.xml` -- long `xml` file specific for this run's job
  - `sched[JOBID].dataset` -- list of MuDSTs associated with this run's job


- `/tmp/[USERNAME]/[JOBID]_[SEG]/`
  - sandbox files
  - anything else the `command` block of the `xml` file copied or linked
    - `.sl* object files directory` (note the issue with different compilers...
      see log 161014)
  - output ROOT files
    - copy block copies ROOT files back  to `output` directory (specified by `xml` file)
  - `.output.log` and `.errror.log` logs the output of the part of csh script; they are 
      later redirected to `stdout` and `stderr` so the `log/*.{out,err}` files contiain them
    - note this redirection happens just before the csh script terminates (look for the line
      where the `$SCRATCH` directory is removed)
  - `.sandboxFiles` is a list used by the csh script to clean up sandbox files at the end
