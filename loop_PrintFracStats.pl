#!/usr/bin/perl -w

my $dir = "hist_pptrans";
my $statfile="${dir}_diag/frac_stats.dat";

open(STAT, "> ${statfile}") or die("ERROR: cannot create ${statfile}");
my $idx = 0;

my @infiles = <${dir}/OFset*.root>;
foreach $file (@infiles) {
  system("root -b -q PrintFracStats.C'(\"${file}\",${idx})'");
  $idx++;
}
close(STAT);
