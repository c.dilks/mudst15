7/11 23:57 -- submitted superset 1 from rcas6006
 - status copied to status.superset.1
 - 26 muDSTs had 'no servers available' issue --> ~2%
 - several jobs were 'aborted' by condor; modified jobCheck.py
   to catch these
7/13 15:07 -- resubmitted 22 runs from rcas6006
 - status copied to status.superset.1.resubmit
 - same 26 muDSTs had 'no servers available' issue
 - other issues resolved though

merging...done
moved ofiles to superset1/
removed merged files
cleanup

7/16 13:35 -- submitted superset 2 from rcas 6006
 - status copied to status.superset.2
 - 83 muDSTS had 'no servers available' issue --> 5.2%
7/17 16:30 -- resubmitted 15 runs from rcas6006
 - status copied to status.superset.2.resubmit
 - same 83 muDSTS failed

merging...done (set 07703 was slow to merge for some reason)
moved ofiles to superset2/
removed merged files
cleanup

7/17 22:50 -- submitted superset 3 from rcas 6006
 - status copied to status.superset.3
 - 47 muDSTS had 'no servers available' issue --> 2.3%
 - 4 runs were on good runlist but not in file catalog
   (RTS probably marked them bad)
 - continuing without resubmit

merging...done
moved ofiles to superset3/
removed merged files
cleanup

7/20 10:45 -- submitted superset 4 from rcas6006
 - status copied to status.superset.4
 - 44 muDSTS had 'no servers available' issue --> 2.2%
 - continuing without resubmit

merging...done
moved ofiles to superset4/
removed merged files
cleanup

7/21 17:27 -- submitted superset 5 from rcas6006
 - status copied to status.superset.5
 - 0 muDSTS had 'no servers available' issue --> 0%
 - continuing without resubmit

merging...done
moved ofiles to superset5/
removed merged files
cleanup
 
7/22 21:07 -- submitted superset 6 from rcas6006
 - status copied to status.superset.6
 - 0 muDSTS had 'no servers available' issue --> 0%
 - continuing without resubmit

merging...done
moved ofiles to superset6/
removed merged files
cleanup

7/23 23:16 -- submitted superset 7 from rcas6006
 - status copied to status.superset.7
 - 0 muDSTS had 'no servers available' issue --> 0%
 - however, run 16092037 gave me some sass
   - 4 jobs failed at submission... the rest were
     left at 'unknown' status
7/24 23:40 -- resubmit run 16092037 from rcas6006
 - status copied to status.superset.7.resubmit
 - ran okay

merging...done
moved ofiles to superset6/
removed merged files
cleanup


>>>>>>>>>>>>>>>>>> DONE MAKING OFILES
however... since we started seeing 0 'no servers available' issues from superset5
onward, let's try to redo supersets 1-4 to see if there is any improvement


7/25 12:16 -- submitted superset 1 from rcas6006
 - a different set of muDSTs failed, but still about 2%
 - some jobs got held, probably long queue stuff
 - I have enough data for now, so cancelling this submission and giving up for now!


--------------------
downloading:
superset1...done
superset2...done
superset3...done
superset4...done
superset5...done
superset6...done
superset7...done


