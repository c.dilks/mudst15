// lists bXing counter and ievt

void CompareEvID(TString filename0="JP2.root",
                 TString filename1="fms_filters_only.root") {
  TString dir = "../hist_pptrans/testing_triggerfiles/";

  filename0 = dir+filename0;
  filename1 = dir+filename1;
  TFile * infile[2];
  infile[0] = new TFile(filename0.Data(),"READ");
  infile[1] = new TFile(filename1.Data(),"READ");

  TTree * tr[2];
  Int_t evid[2];
  UInt_t bc[2][2];

  for(int k=0; k<2; k++) {
    tr[k] = (TTree*) infile[k]->Get("evtr");
    tr[k]->SetBranchAddress("evid",&(evid[k]));
    tr[k]->SetBranchAddress("bc",bc[k]);
  };


  Int_t nbins = 4000;
  TH2D * h[2];
  h[0] = new TH2D("h0","h0",nbins,0,nbins,nbins,0,nbins);
  h[1] = new TH2D("h1","h1",nbins,0,nbins,nbins,0,nbins);
  TH2D * d = new TH2D("d","d",nbins,0,nbins,nbins,0,nbins);
  TH2D * s = new TH2D("s","s",nbins,0,nbins,nbins,0,nbins);

  Int_t evid_max=0;


  for(int k=0; k<2; k++) {
    for(int x=0; x<tr[k]->GetEntries(); x++) {
      tr[k]->GetEntry(x);
      h[k]->SetBinContent(evid[k],1);
      evid_max = evid[k]>evid_max ? evid[k]:evid_max;
    };
  };
  printf("evid_max=%d\n",evid_max);

  d->Add(h[0],h[1],1,-1);
  s->Add(h[0],h[1],1,1);

  TH1D * d_h = new TH1D("d_h","d_h",3,-1,2);
  TH1D * s_h = new TH1D("s_h","s_h",3,0,3);
  Int_t d_y,s_y;
  Int_t tot=0;
  for(int x=1; x<evid_max; x++) {
    d_y=d->GetBinContent(x);
    s_y=s->GetBinContent(x);
    if(s_y>0) {
      d_h->Fill(d_y);
      s_h->Fill(s_y);
      tot++;
    };
  };

  d_h->Scale(1.0/tot);
  s_h->Scale(1.0/tot);

  printf("\n");
  printf("%.2f%% of all %d events are in %s but not in %s\n",100*(d_h->GetBinContent(1)),tot,filename1.Data(),filename0.Data());
  printf("%.2f%% of all %d events are in %s but not in %s\n",100*(d_h->GetBinContent(3)),tot,filename0.Data(),filename1.Data());
  printf("%.2f%% of events in both\n",100*(s_h->GetBinContent(3)));
  printf("\n");

    
  d->Draw("colz");
  /*
  new TCanvas();
  d_h->Draw();
  new TCanvas();
  s_h->Draw();
  */
};
