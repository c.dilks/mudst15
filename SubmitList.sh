#!/bin/bash
# based off oleg eyser's code
#
# takes list of runs and runs 'star-submit' on them
# note: only the first column of your run list needs to be a run number, this
#       script does not care about additional columns

###############################
production="P15ik"
stream="st_fms"
###############################

if [ $# -ne 1 ]; then
  echo "usage: $0 [run_list]"
  exit 1
fi
list=$1

# make file with columns [run#] [trg setup]
trgfile="lists/trigger_pp.dat"
trgfileawk="${trgfile}.awk"
cat $trgfile | awk '{print $2" "$5}' > $trgfileawk


while read line; do
  runnum=$(echo $line | awk '{print $1}')
  trgset="$(grep $runnum $trgfileawk | awk '{print $2}')"
  if [ -z "$trgset" ]; then
    error="ERROR $runnum -------------------------------> "
    error="${error}trigger configuration not found in trigger.dat"
    echo "$error"; echo
    continue
  fi

  echo "SUBMIT $runnum $production $trgset $stream"
  star-submit-template -template template_scheduler.xml \
    -entities RUNNUMBER=$runnum,SPROD=$production,STRIG=$trgset,SNAME=$stream
  echo
  #sleep 1
done < $list
